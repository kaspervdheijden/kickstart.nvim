#!/usr/bin/env sh

cd "$(git rev-parse --show-toplevel 2>/dev/null)" || {
    printf 'Could not enter root dir\n' >&2
    exit 1
}

action="$(printf 'enable\ndisable\n' | grep -E "^${1}")"
if [ -z "${1}" ] || [ -z "${action}" ]; then
    printf 'Usage: %s <enable|disable> [plugin]\n\n' "$(basename "${0}")" >&2
    exit "$([ -z "${1}" ]; printf '%d' "${?}")"
fi

files="$(find ./lua -maxdepth 1 -type d -name 'plugins-*')"
trg="$(printf '%s' "${files}" | grep -F "/plugins-${action}")"
src="$(printf '%s' "${files}" | grep -vF "${trg}")"

if [ -z "${2}" ]; then
    find "${src}" -maxdepth 1 -type f -name '*.lua' -print0 | xargs -0 -I{} basename '{}' | sed 's/\.lua//'
    exit
fi

while [ -n "${2}" ]; do
    file="${2}"
    shift

    if [ ! -f "${file}" ]; then
        file="$(find "${src}" -type f -name "${file}*.lua")"
    fi

    if [ "$(printf '%s\n' "${file}" | wc -l)" -gt 1 ]; then
        printf 'Could not %s "%s" (not unique: ["%s"])\n' "${action}" "${1}" "$(printf '%s' "${file}" | xargs -I{} basename '{}' | sed 's/\.lua//' | xargs | sed 's/ /", "/g')" >&2
    elif [ ! -f "${file}" ]; then
        printf 'Could not %s "%s" (file not found)\n' "${action}" "${1}" >&2
    else
        printf '%s %s\n' "${action}" "$(printf '%s' "${file##*/}" | sed 's/\.lua$//')"
        mv "${file}" "${trg}"
    fi
done
