#!/usr/bin/env sh

err() {
    if [ -n "${1}" ]; then
        printf '\033[1;31m%s\033[0;0m\n' "${1}" >&2
    fi

    printf '\nPress q to quit\n\n\n' | env LESS='' LESSHISTFILE='/dev/null' less
    exit 1
}

file="${2:-.}"
case "${1}" in
    add|add-interactive)
        if [ -z "$(git diff "${file}")" ]; then
            err "No hunks in '${file}'"
        fi

        ;;
    restore|restore-interactive)
        if [ -z "$(git diff --staged "${file}")" ]; then
            err "No staged hunks in '${file}'"
        fi

        ;;
esac

case "${1}" in
    restore-interactive) git reset -p "${file}"         || err ;;
    add-interactive)     git add -p "${file}"           || err ;;
    checkout)            git checkout -- "${file}"      || err ;;
    restore)             git restore "${file}"          || err ;;
    add)                 git add "${file}"              || err ;;
    switch-branch)
        if command -v toolgit >/dev/null; then
            exec toolgit branch checkout
        fi

        branches="$(git branch | grep -vF '* ')"
        if [ -z "${branches}" ]; then
            err 'No branches'
        fi

        if ! command -v fzf >/dev/null; then
            err 'No fzf'
        fi

        chosen="$(printf '%s\n' "${branches}" | fzf)"
        if [ -n "${chosen}" ]; then
            git checkout "${chosen}" || err
        fi

        ;;
    branch-name)
        git symbolic-ref --short HEAD 2>/dev/null | tr -d '\n'
        ;;
    update)
        if command -v toolgit >/dev/null; then
            toolgit pull
        else
            "$(dirname "${0}")./update.sh"
        fi


        if [ -n "${file}" ] && [ -x "${file}" ] && printf '%s' "${file}" | grep -qE '(ba|fi|z)?sh$'; then
            exec "${file}"
        fi

        ;;
    remote-url)
        git remote -v | awk '/fetch/ { print $2 }' | tail -n1 | sed 's#^git@#https://#; s/.git$//; s#:#/#g; s#///#://#'
        ;;
esac
