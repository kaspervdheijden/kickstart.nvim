#!/usr/bin/env sh

dir="${1}"
if [ -z "${dir}" ]; then
    printf 'No directory given\n' >&2
    exit 7
fi

if [ -f "${dir}" ]; then
    dir="$(dirname "${dir}")"
fi

if ! cd "${dir}"; then
    printf 'Could not enter directory "%s"\n' "${dir}" >&2
    exit 8
fi

if [ -n "${TMUX}" ]; then
    num_panes="$(tmux list-panes | wc -l)"

    if [ "$(( num_panes % 2 ))" -eq 1 ]; then
        exec tmux split-window -v
    else
        exec tmux split-window -h
    fi
fi

exec "$(dirname "${0}")/shell.sh"
