#!/usr/bin/env sh

if ! command -v tmux >/dev/null; then
    printf 'Binary not found: tmux\n' >&2
    exit 127
fi

if [ -z "${TMUX}" ]; then
    if [ -z "${1}" ]; then
        printf 'Not currently in a tmux session and no command given\n' >&2
        exit 1
    fi

    exec "${@}"
fi

num_panes="$(tmux list-panes | wc -l)"
if [ "$(( num_panes % 2 ))" -eq 1 ]; then
    exec tmux split-window -v -c "#{pane_current_path}" "${@}"
else
    exec tmux split-window -h -c "#{pane_current_path}" "${@}"
fi
