#!/usr/bin/env sh

if ! cd "$(git rev-parse --show-toplevel 2>/dev/null)"; then
    printf 'Directory is not under git control: "%s"\n' "$(pwd)" >&2
    exec "$(dirname "${0}")/shell.sh"
fi

if [ -f ./.git/index.lock ]; then
    if pgrep git >/dev/null; then
        printf 'Git is running\n' >&2
        exec "$(dirname "${0}")/shell.sh"
    fi

    rm ./.git/index.lock
fi

remote="$(git remote -v | grep -Eq '^upstream\s' && printf 'upstream\n' || printf 'origin\n')"
cur_branch="$(git symbolic-ref --short HEAD 2>/dev/null)"
branches="$(git branch | cut -c3-)"
main_branch="${cur_branch}"

for candidate in master main trunk; do
    if [ "$(printf '%s\n' "${branches}" | awk -v branch="${candidate}" '$0 == branch { print; exit; }')" = "${candidate}" ]; then
        main_branch="${candidate}"
        break
    fi
done

if [ "${cur_branch}" != "${main_branch}" ]; then
    if ! git checkout "${main_branch}"; then
        printf 'Error changing branch, aborting.\n' >&2
        exec "$(dirname "${0}")/shell.sh"
    fi
fi

printf '\033[1;32mgit pull %s %s\033[0;0m\n' "${remote}" "${main_branch}"

hash="$(sha1sum ./composer.json ./composer.lock 2>/dev/null | cut -c -40 | tr -d '\n')"
git -c 'color.status=always' pull "${remote}" "${main_branch}" 2>&1 | grep -vE '^From| \* '

if [ "${cur_branch}" != "${main_branch}" ]; then
    git checkout "${cur_branch}"
    # Perhaps (offer to) rebase?
    # git rebase "${main_branch}"
fi

git log --date='format:%Y-%m-%d %k:%M' --pretty='format:%C(auto)%h: %s ON %aD (%cr) by %an' -1
if [ "${hash}" != "$(sha1sum ./composer.json ./composer.lock 2>/dev/null | cut -c -40 | tr -d '\n')" ]; then
    composer install
elif [ -n "${hash}" ]; then
    printf 'No need to composer install\n'
fi

exec "$(dirname "${0}")/shell.sh"
