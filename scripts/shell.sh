#!/usr/bin/env sh

shl=''
if [ -f /etc/passwd ]; then
    shl="$(awk -F: -v user="$(whoami)" '$1 == user { print $NF }' /etc/passwd)"
fi

if [ -z "${shl}" ]; then
    shl="${SHELL}"
fi

if [ -z "${shl}" ]; then
    shl="$(command -v zsh || command -v bash)"

    if [ -z "${shl}" ]; then
        printf 'Default shell cannot be determined\n' >&2
        exit 5
    fi
fi

exec "${shl}"
