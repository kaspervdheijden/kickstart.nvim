# Kickstart.nvim

![Dashboard](resource/screenshot.png)

This configuration can be configured using these environment variables:

- `MOUSE` / `NVIM_MOUSE`
    Disable mouse when set to `off`.

- `DF_FLAGS`
    Disable mouse when "M" flag is set.

- `NVIM_COLORSCHEME`
    Colorscheme to use.

- `NVIM_CUSTOM_COMMANDS`
    Custom commands, seperated by `=` chars. Each field has 5 fields, separated by colons:
    `modes:key map:command:description:flags`

    `modes`: `i`, `n`, `v`, `t` or combinations
    `keymap`: Any valid key mapping
    `command`: The shell command to run
    `description`: A short description
    `flags`: h=hold (keep terminal open after command), w=wrap (use cmd.sh to choose a tmux pane) or p=popup (popup window)
            These can be combined

    Example:
    `inv:<C-t>:top:Run top:w=inv:<C-\>r:git pull && composer install:Update repo:h`

- `NVIM_HEADER_FILE`
    File that has the greeter header. Some variables are supported:
        `DIRNAME`: Current directory name
        `USER`: Current user name
        `VERSION`: Neovim version
        `DIR`: Current full directory path

- `NVIM_CUSTOM_SNIPPETS_DIR`
    Custom snippets location, defaults to `~/.config/dotfiles-local-config/snippets`

- `NVIM_IGNORED_LSP_LABELS`
    List of ignored labels from LSP for autocompletion, delimited by `:`. If there are many, you can put them in a file and
    set the `NVIM_IGNORED_LSP_LABELS_FILE` variable to point to that

- `NVIM_LSP_INTELEPHENSE_LICENCEKEY`
    The variable to hold the licence key for the intelephense PHP lsp.
