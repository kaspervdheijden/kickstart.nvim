return {
    'williamboman/mason-lspconfig.nvim',
    event        = 'BufReadPost',
    dependencies = {
        'neovim/nvim-lspconfig',
    },
    config = function ()
        local cfg = require('component.configurator')
        local lsp = require('component.lsp')

        local on_attach = function (client, bufnr)
            cfg.keymaps({
                { 'n',               '<leader>cm', vim.lsp.buf.rename,         buffer = bufnr, desc = 'LSP: Rename',                  },
                { 'n',               '<leader>ca', vim.lsp.buf.code_action,    buffer = bufnr, desc = 'LSP: Code action'              },
                { { 'i', 'n', 'v' }, '<M-CR>',     vim.lsp.buf.code_action,    buffer = bufnr, desc = 'LSP: Code action',             },
                { 'n',               'gd',         lsp.definitions,            buffer = bufnr, desc = 'LSP: Goto definition',         },
                { 'n',               'gr',         lsp.references,             buffer = bufnr, desc = 'LSP: Goto references',         },
                { 'n',               'gt',         lsp.implementations,        buffer = bufnr, desc = 'LSP: Goto implementation',     },
                { 'n',               'gc',         vim.lsp.buf.declaration,    buffer = bufnr, desc = 'LSP: Goto declaration',        },
                { 'n',               'gf',         lsp.type_definitions,       buffer = bufnr, desc = 'LSP: Goto type definition',    },
                { 'n',               '<leader>sp', lsp.project_symbols,        buffer = bufnr, desc = 'LSP: Workspace symbols',       },
                { 'n',               '<leader>sd', lsp.document_symbols,       buffer = bufnr, desc = 'LSP: Document symbols',        },
                { 'n',               '<leader>cd', lsp.diagnostics,            buffer = bufnr, desc = 'Diagnostics',                  },
                { 'n',               '<leader>sq', lsp.quickfix,               buffer = bufnr, desc = 'Quickfix',                     },
                { 'n',               '<leader>cr', lsp.restart,                buffer = bufnr, desc = 'LSP: Restart',                 },
                { 'n',               '<F5>',       lsp.restart,                buffer = bufnr, desc = 'LSP: Restart',                 },
                { { 'i', 'n', 'v' }, '<C-k>',      vim.lsp.buf.signature_help, buffer = bufnr, desc = 'LSP: Signature documentation', }, -- See `:help K` for why this keymap
                { { 'i', 'n', 'v' }, '<C-l>',      vim.lsp.buf.hover,          buffer = bufnr, desc = 'LSP: Hover documentation',     },
                { 'n',               'K',          vim.lsp.buf.hover,          buffer = bufnr, desc = 'LSP: Hover documentation',     },
                { { 'n', 'v' },      '[d',         lsp.diagnostics_prev,       buffer = bufnr, desc = 'Previous diagnostic'           },
                { { 'n', 'v' },      ']d',         lsp.diagnostics_next,       buffer = bufnr, desc = 'Next diagnostic'               },
            })

            if client.server_capabilities.documentHighlightProvider then
                cfg.autocmds(
                    {
                        {
                            { 'CursorHold', 'CursorHoldI' },
                            vim.lsp.buf.document_highlight,
                            buffer = bufnr,
                        },
                        {
                            { 'CursorMoved', 'CursorMovedI' },
                            vim.lsp.buf.clear_references,
                            buffer = bufnr,
                        },
                    },
                    'lsp-doc-highlight'
                )
            end
        end

        local lsp_config   = require('lspconfig')
        local capabilities = lsp.capabilities()
        local servers      = lsp.servers()

        for lsp_name, config in pairs(servers) do
            config.telemetry = { enabled = false }

            lsp_config[lsp_name].setup({
                init_options = config.init_options,
                filetypes    = config.filetypes,
                capabilities = capabilities,
                on_attach    = on_attach,
                settings     = config,
            })
        end
    end,
}
