return (function (remote)
    return {
        'https://gitlab.com/kaspervdheijden/simple-rsync.nvim.git',
        event = 'VeryLazy',
        cond  = remote ~= '',
        keys  = {
            { '<leader>=d', function () require('simple-rsync').download(vim.fn.expand('%:.')) end, desc = 'Download current buffer' },
            { '<leader>=u', function () require('simple-rsync').upload(vim.fn.expand('%:.'))   end, desc = 'Upload current buffer' },
            { '<leader>=D', function () require('simple-rsync').download('./')                 end, desc = 'Download all' },
            { '<leader>=U', function () require('simple-rsync').upload('./')                   end, desc = 'Upload all' },
        },
        opts  = function ()
            require('component.configurator').register_key_groups({ { '<leader>=', 'Rsync' } })

            return {
                args    = vim.env.NVIM_RSYNC_FLAGS,
                logfile = vim.env.NVIM_RSYNC_LOG,
                remote  = remote,
            }
        end,
    }
end)(vim.env.NVIM_RSYNC_REMOTE or '')
