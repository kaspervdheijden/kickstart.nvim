return {
    'hrsh7th/nvim-cmp',
    event        = { 'InsertEnter', 'CmdlineEnter' },
    version      = false,
    dependencies = {
        'saadparwaiz1/cmp_luasnip',
        'hrsh7th/cmp-nvim-lsp',
        'hrsh7th/cmp-buffer',
        -- 'hrsh7th/cmp-path',
        'L3MON4D3/LuaSnip',
    },
    config = function ()
        local cfg     = require('component.configurator')
        local luasnip = require('luasnip')
        local cmp     = require('cmp')

        require('luasnip.loaders.from_vscode').lazy_load({
            paths = {
                vim.env.NVIM_CUSTOM_SNIPPETS_DIR or '~/.config/dotfiles-local-config/snippets',
                './snippets',
            },
        })

        luasnip.config.setup({})

        cfg.keymaps({
            {
                'i',
                '<A-Left>',
                function ()
                    if luasnip.locally_jumpable(-1) then
                        luasnip.jump(-1)
                    end
                end,
                desc   = 'Jump to previous snippet insert point',
                silent = true,
            },
            {
                'i',
                '<A-Right>',
                function ()
                    if luasnip.locally_jumpable() then
                        luasnip.jump()
                    end
                end,
                desc   = 'Jump to next snippet insert point',
                silent = true,
            },
        })

        local bordered = cmp.config.window.bordered()
        local ignored  = {}
        local icons    = {
            Array         = ' ',
            Boolean       = '󰨙 ',
            Class         = ' ',
            Codeium       = '󰘦 ',
            Color         = ' ',
            Control       = ' ',
            Collapsed     = ' ',
            Constant      = '󰏿 ',
            Constructor   = ' ',
            Copilot       = ' ',
            Enum          = ' ',
            EnumMember    = ' ',
            Event         = ' ',
            Field         = ' ',
            File          = ' ',
            Folder        = ' ',
            Function      = '󰊕 ',
            Interface     = ' ',
            Key           = ' ',
            Keyword       = ' ',
            Method        = '󰊕 ',
            Module        = ' ',
            Namespace     = '󰦮 ',
            Null          = ' ',
            Number        = '󰎠 ',
            Object        = ' ',
            Operator      = ' ',
            Package       = ' ',
            Property      = ' ',
            Reference     = ' ',
            Snippet       = ' ',
            String        = ' ',
            Struct        = '󰆼 ',
            TabNine       = '󰏚 ',
            Text          = ' ',
            TypeParameter = ' ',
            Unit          = ' ',
            Value         = ' ',
            Variable      = '󰀫 ',
        }

        if vim.env.NVIM_IGNORED_LSP_LABELS_FILE ~= nil then
           local file = io.open(vim.fn.expand(vim.env.NVIM_IGNORED_LSP_LABELS_FILE), 'r')
            if file then
                for label in file:lines() do
                   if label ~= '' then
                        ignored[label] = 1
                    end
                end

                file:close()
            end
        end

        for _, label in ipairs(vim.split(vim.env.NVIM_IGNORED_LSP_LABELS or '', ':')) do
            if label ~= '' then
                ignored[label] = 1
            end
        end

        ---@diagnostic disable-next-line: redundant-parameter
        cmp.setup({
            completion = { completeopt = 'menu,menuone,noinsert' },
            snippet    = {
                expand = function (args)
                    luasnip.lsp_expand(args.body)
                end,
            },
            window  = { documentation = bordered, completion = bordered },
            mapping = cmp.mapping.preset.insert({
                ['<C-y>']     = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Insert, select = true }),
                ['<CR>']      = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Insert, select = true }),
                ['<C-n>']     = cmp.mapping.select_next_item(),
                ['<C-p>']     = cmp.mapping.select_prev_item(),
                ['<Tab>']     = cmp.mapping.select_next_item(),
                ['<S-Tab>']   = cmp.mapping.select_prev_item(),
                ['<C-d>']     = cmp.mapping.scroll_docs(-4),
                ['<C-f>']     = cmp.mapping.scroll_docs(4),
                ['<C-Space>'] = cmp.mapping.complete({}),
                ['<C-e>']     = cmp.mapping.abort(),
            }),
            sources = {
                {
                    name         = 'nvim_lsp',
                    group_index  = 1,
                    entry_filter = function (entry)
                        -- HACK: Is there no easier way to override a suggested variable name from the LSP?
                        return ignored[entry:get_completion_item().label] == nil
                    end,
                },
                { name = 'luasnip',  group_index = 1 },
                { name = 'buffer',   group_index = 1 },
                { name = 'path',     group_index = 1, keyword_length = 2, },
            },
            formatting = {
                format = function (_, item)
                    if icons[item.kind] then
                        item.kind = icons[item.kind] .. item.kind
                    end

                    return item
                end,
            },
            experimental = {
                ghost_text = false,
            },
        })

        local style = require('config.style')
        cfg.set_hls({
            { 'CmpItemAbbrMatchFuzzy', bg = style.default_bg, fg = style.colors.highlight1 },
            { 'CmpItemAbbrMatch',      bg = style.default_bg, fg = style.colors.highlight1 },
        })
    end,
}
