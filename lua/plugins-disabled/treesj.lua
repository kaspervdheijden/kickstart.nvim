return {
    'Wansmer/treesj',
    dependencies = { 'nvim-treesitter/nvim-treesitter' },
    event        = { 'BufReadPost', 'BufNewFile' },
    keys         = {
        {
            '<leader>cj',
            function ()
                require('treesj').toggle()
            end,
            desc = 'Toggle Treesitter join',
        },
    },
    opts = {
        use_default_keymaps = false,
        max_join_length     = 255,
    },
}
