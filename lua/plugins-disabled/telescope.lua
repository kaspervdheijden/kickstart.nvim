return {
    'nvim-telescope/telescope.nvim',
    cond         =  require('component.picker').implementor == 'telescope',
    cmd          = 'Telescope',
    branch       = '0.1.x',
    dependencies = {
        'nvim-lua/plenary.nvim',
        {
            'nvim-telescope/telescope-fzf-native.nvim',
            cond   = vim.fn.executable('make') == 1,
            build  = 'make',
            config = function ()
                require('telescope').load_extension('fzf')
            end,
        },
        {
            'mollerhoj/telescope-recent-files.nvim',
            config = function ()
                require('telescope').load_extension('recent-files')
            end
        },
    },
    config = function ()
        local style     = require('config.style').border
        local actions   = require('telescope.actions')
        local telescope = require('telescope')
        local chars     = nil

        if style ~= 'rounded' and style ~= 'none' then
            chars = { '─', '│', '─', '│', '┌', '┐', '┘', '└' }
        end

        telescope.setup({
            defaults = {
                layout_config        = {
                    prompt_position  = 'top',
                    preview_width    = 0.4,
                    height           = 0.8,
                    width            = 0.9,
                },
                file_ignore_patterns = {
                    'node_modules',
                    'build/',
                    '%.png',
                    '.git/',
                },
                border               = style ~= 'none',
                sorting_strategy     = 'ascending',
                borderchars          = chars,
                prompt_prefix        = ' ',
                selection_caret      = ' ',
                winblend             = 0,
                vimgrep_arguments    = {
                    'rg',
                    '--sortr=modified',
                    '--with-filename',
                    '--color=never',
                    '--ignore-case',
                    '--no-heading',
                    '--hidden',
                    '--column',
                    '--trim',
                },
                mappings = {
                    i = {
                        ['<C-s>'] = actions.file_vsplit,
                        ['<C-v>'] = actions.file_split,
                        ['<ESC>'] = actions.close,
                    },
                },
            },
            pickers = {
                lsp_dynamic_document_symbols = { fname_width = 60 },
                lsp_type_definitions         = { fname_width = 60 },
                lsp_implementations          = { fname_width = 60 },
                lsp_definitions              = { fname_width = 60 },
                lsp_references               = { fname_width = 60 },
                lsp_document_symbols         = {
                    fname_width = 60,
                    symbols     = {
                        'constant',
                        'property',
                        'method',
                    },
                },
                find_files = {
                    find_command = {
                        'rg',
                        '--sortr=modified',
                        '--color=never',
                        '--ignore-case',
                        '--no-heading',
                        '--hidden',
                        '--column',
                        '--trim',
                    },
                },
                buffers = {
                    mappings = {
                        i = {
                            ['<C-d>'] = actions.delete_buffer + actions.move_to_top,
                        },
                    },
                },
            },
        })
    end,
}
