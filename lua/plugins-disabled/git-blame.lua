return {
    'f-person/git-blame.nvim',
    event = 'VeryLazy',
    opts  = {
        message_template           = '<author> • <summary> • <date> • <sha>',
        set_extmark_options        = { virt_text_pos = 'right_align' },
        highlight_group            = 'BufferLineDuplicateVisible',
        date_format                = '%r (%Y-%m-%d %H:%M)',
        message_when_not_committed = '(Uncommitted)',
    },
}
