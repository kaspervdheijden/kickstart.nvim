return {
    'folke/which-key.nvim',
    event  = 'VeryLazy',
    -- cond   = (vim.env.NVIM_ENABLE_WHICHKEY or '0') == '1',
    opts = {
        delay  = function (ctx) return ctx.plugin and 0 or 1000 end,
        preset = 'helix',
        icons  = {
            rules = false,
            group = '',
        },
    },
}
