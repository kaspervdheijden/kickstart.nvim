return {
    'folke/noice.nvim',
    event        = function ()
        vim.defer_fn(function () require('noice') end, 100)
        return { 'InsertEnter' }
    end,
    dependencies = {
        'MunifTanjim/nui.nvim',
        -- {
        --     'rcarriga/nvim-notify',
        --     config = function ()
        --         local notify = require('notify')
        --
        --         ---@diagnostic disable-next-line: undefined-field
        --         notify.setup({
        --             background_colour = require('config.style').colors.dark0,
        --             level             = vim.log.levels.INFO,
        --             render            = 'minimal',
        --             stages            = 'static',
        --             timeout           = 4000,
        --         })
        --
        --         vim.lsp.handlers['window/showMessage'] = function (_, method, params, client_id)
        --             ---@diagnostic disable-next-line: undefined-field
        --             local lvl = ({ 'error', 'warn', 'info', 'info' })[params.type] or 'info'
        --
        --             notify(
        --                 { method.message },
        --                 lvl,
        --                 ---@diagnostic disable-next-line: redundant-parameter
        --                 {
        --                     ---@diagnostic disable-next-line: param-type-mismatch
        --                     title   = 'LSP | ' ..  vim.lsp.get_client_by_id(client_id).name,
        --                     timeout = 5000,
        --                     keep    = function ()
        --                         return lvl == 'error' or lvl == 'warn'
        --                     end,
        --                 }
        --             )
        --         end
        --     end
        -- },
    },
    opts = {
        lsp = {
            override = {
                ['vim.lsp.util.convert_input_to_markdown_lines'] = true,
                ['vim.lsp.util.stylize_markdown']                = true,
                ['cmp.entry.get_documentation']                  = true,
            },
        },
        views = {
            mini = {
                win_options = { winblend = 0 },
            },
        },
        routes = {
            {
                filter = {
                    event = 'msg_show',
                    any   = {
                        { find = '%d+ lines <ed %d+ time' },
                        { find = '%d+ lines >ed %d+ time' },
                        { find = '; before #%d+' },
                        { find = '; after #%d+' },
                        { find = '%d+L, %d+B' },
                    },
                },
                view = 'mini',
            },
            {
                opts   = { skip = true },
                filter = {
                    event = 'msg_show',
                    any   = {
                        { find = 'Col %d+-%d+ of %d+-%d-; Line %d+ of %d+; Word %d+ of %d+; Char %d+ of %d+; Byte %d+ of %d+' },
                        { find = [[Empty file name for '%' or '#', only works with ":p:h"]] },
                        { find = "Cannot make changes, 'modifiable' is off" },
                        { find = "Cannot write, 'buftype' option is set" },
                        { find = 'Cannot close last window' },
                        { find = '%d+ lines indented' },
                        { find = '%d+ lines moved' },
                        { find = 'tag stack empty' },
                    },
                },
            },
        },
        presets = {
            long_message_to_split = true,
            command_palette       = true,
            bottom_search         = true,
            inc_rename            = true,
        },
    },
}
