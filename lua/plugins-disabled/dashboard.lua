return {
    'nvimdev/dashboard-nvim',
    cond  = vim.fn.argc() == 0,
    event = 'UIEnter',
    keys  = {
        {
            '<leader>ad',
            function ()
                require('component.greeter').opener(function () require('dashboard'):instance() end)()
            end,
            desc = 'Close all buffers and open dashboard',
        },
    },
    config = function ()
        local cfg = require('component.configurator')

        cfg.autocmds(
            {
                {
                    { 'FileType', 'BufEnter' },
                    function ()
                        if vim.bo.filetype ~= 'dashboard' then
                            return
                        end

                        cfg.set_hl({ 'DashboardHeader', fg = 'darkGray', bold = true, })

                        vim.b.minicursorword_disable  = true
                        vim.b.miniindentscope_disable = true
                        vim.b.minitrailspace_disable  = true
                    end,
                    desc    = 'Disable cursor word and trailspace highlighting',
                    pattern = 'dashboard',
                },
                {
                    'User',
                    function ()
                        local ok, mini = pcall(require, 'mini.trailspace')
                        if ok and mini ~= nil then
                            mini.unhighlight()
                        end
                    end,
                    pattern = 'DashboardLoaded',
                },
            },
            'dashboard-disable-minicursorword'
        )

        local greeter   = require('component.greeter')
        local dashboard = require('dashboard')
        local items     = greeter.items(function () dashboard:instance() end)

        greeter.set_handlers_to_lauch_after_last_buffer_close(
            'dashboard',
            function ()
                dashboard:instance()
            end
        )

        dashboard.setup({
            theme  = 'doom',
            config = {
                header = (function (calc_header_height, center)
                    local headers = greeter.headers(calc_header_height)

                    if not calc_header_height then
                        return headers
                    end

                    local total_height         = vim.api.nvim_win_get_height(0)
                    local target_header_height = math.ceil((total_height - #center * 2) / 2)
                    local header_height        = #headers

                    if header_height >= target_header_height then
                        target_header_height = header_height + 5
                    end

                    local lines_to_add = target_header_height - header_height
                    if lines_to_add > 2 then
                        lines_to_add = lines_to_add - 2
                        table.insert(headers, '')
                        table.insert(headers, '')
                    end

                    for _ = 1, lines_to_add do
                        table.insert(headers, 1, '')
                    end

                    return headers
                end)(true, items),
                footer = greeter.footer,
                center = (function (len, center)
                    local elements, buffer = {}, string.rep(' ', len)

                    for _, item in ipairs(center) do
                        if item[1] ~= nil then
                            table.insert(elements, {
                                desc       = string.sub('  ' .. (item[1] or '') .. buffer, 1, len),
                                action     = item[2],
                                key        = item[3],
                                key_format = '%s',
                            })
                        end
                    end

                    return elements
                end)(45, items),
            },
        })
    end,
}
