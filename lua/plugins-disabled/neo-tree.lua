return {
    'nvim-neo-tree/neo-tree.nvim',
    cmd          = 'Neotree',
    branch       = 'v3.x',
    dependencies = {
      'nvim-lua/plenary.nvim',
      -- 'nvim-tree/nvim-web-devicons',
      'MunifTanjim/nui.nvim',
    },
    config = function ()
        local handlers = {
            {
                event   = 'file_added',
                handler = function (path)
                    if vim.fn.isdirectory(path) == 1 then
                        return
                    end

                    vim.defer_fn(
                        function ()
                            vim.api.nvim_command('e ' .. path)
                        end,
                        200
                    )
                end,
            },
        }

        -- If you want icons for diagnostic errors, you'll need to define them somewhere:
        vim.fn.sign_define('DiagnosticSignError', { text = ' ', texthl = 'DiagnosticSignError' })
        vim.fn.sign_define('DiagnosticSignWarn',  { text = ' ', texthl = 'DiagnosticSignWarn'  })
        vim.fn.sign_define('DiagnosticSignInfo',  { text = ' ', texthl = 'DiagnosticSignInfo'  })
        vim.fn.sign_define('DiagnosticSignHint',  { text = '󰌵 ', texthl = 'DiagnosticSignHint'  })

        require('neo-tree').setup({
            open_files_do_not_replace_types = { 'terminal', 'trouble', 'qf' }, -- When opening files, do not use windows containing these filetypes or buftypes
            popup_border_style              = require('config.style').border,
            event_handlers                  = handlers,
            sort_case_insensitive           = false,
            close_if_last_window            = false,
            enable_diagnostics              = true,
            enable_git_status               = true,
            sort_function                   = nil,
            default_component_configs       = {
                container = { enable_character_fade = true, },
                indent    = {
                    highlight           = 'NeoTreeIndentMarker',
                    expander_highlight  = 'NeoTreeExpander',
                    with_markers        = true,
                    with_expanders      = nil,
                    indent_marker       = '│',
                    last_indent_marker  = '└',
                    expander_collapsed  = '',
                    expander_expanded   = '',
                    indent_size         = 2,
                    padding             = 1,
                },
                icon = {
                    highlight     = 'NeoTreeFileIcon',
                    folder_closed = '',
                    folder_open   = '',
                    folder_empty  = '󰜌',
                    default       = '*',
                },
                modified = {
                    highlight = 'NeoTreeModified',
                    symbol    = '[+]',
                },
                name = {
                    highlight             = 'NeoTreeFileName',
                    trailing_slash        = false,
                    use_git_status_colors = true,
                },
                git_status = {
                    symbols = {
                        -- Change type
                        added     = '', -- Or '✚', but this is redundant info if you use git_status_colors on the name
                        modified  = '', -- Or '', but this is redundant info if you use git_status_colors on the name
                        deleted   = '✖',-- This can only be used in the git_status source
                        renamed   = '󰁕',-- This can only be used in the git_status source
                        -- Status type
                        untracked = '',
                        ignored   = '',
                        unstaged  = '󰄱',
                        staged    = '',
                        conflict  = '',
                    },
                },
                -- If you don't want to use these columns, you can set `enabled = false` for each of them individually
                file_size = {
                    enabled        = true,
                    required_width = 64,  -- Min width of window required to show this column
                },
                last_modified = {
                    enabled        = true,
                    required_width = 88,  -- Min width of window required to show this column
                },
                created = {
                    enabled        = true,
                    required_width = 110, -- Min width of window required to show this column
                },
                type = {
                    enabled        = true,
                    required_width = 122, -- Min width of window required to show this column
                },
                symlink_target = {
                    enabled = false,
                },
            },
            commands = {
                nop = function () end,
                wqa = function () vim.api.nvim_command('wqa!') end,
                qa  = function () vim.api.nvim_command('qa!')  end,
            },
            window   = {
                position        = 'left',
                width           = 55,
                mapping_options = {
                    noremap = true,
                    nowait  = true,
                },
                mappings = {
                    ['p']             = 'paste_from_clipboard',
                    ['X']             = 'close_all_subnodes',
                    ['y']             = 'copy_to_clipboard',
                    ['i']             = 'show_file_details',
                    ['<Space>']       = 'toggle_node',
                   -- ['E']             = 'expand_all_nodes',
                   -- ['e']             = 'close_all_nodes',
                   -- ['q']             = 'close_window',
                    ['h']             = 'open_vsplit',
                    ['v']             = 'open_split',
                    ['x']             = 'close_node',
                    ['?']             = 'show_help',
                    ['R']             = 'refresh',
                    ['d']             = 'delete',
                    ['r']             = 'rename',
                    ['<2-LeftMouse>'] = 'open',
                    ['<C-n>']         = 'open',
                    ['<CR>']          = 'open',
                    ['m']             = 'move',
                    ['c']             = 'copy',
                    ['>']             = 'nop',
                    ['<']             = 'nop',
                    ['P']             = 'nop',
                    ['t']             = 'nop',
                    ['.']             = 'nop',
                    ['[g']            = 'nop',
                    [']g']            = 'nop',
                    ['l']             = 'nop',
                    ['#']             = 'nop',
                    ['A']             = 'nop',
                    ['ZZ']            = 'wqa',
                    ['ZQ']            = 'qa',
                    ['a']             = {
                        'add',
                        config = {
                            show_path = 'none' -- 'none', 'relative', 'absolute'
                        },
                    },
                    ['<BS>'] = function (state)
                        require('neo-tree.ui.renderer').focus_node(
                            state,
                            state.tree:get_node():get_parent_id()
                        )
                    end,
                },
            },
            nesting_rules = {},
            filesystem    = {
                filtered_items = {
                    never_show            = { '.DS_Store', '.git', '.idea' }, -- Remains hidden even if visible is toggled to true, this overrides always_show
                    always_show           = { '.gitignore', },
                    hide_by_pattern       = { '.*.cache' },
                    hide_by_name          = { 'build', },
                    hide_gitignored       = false,
                    hide_dotfiles         = false,
                    visible               = false, -- When true, they will just be displayed differently than normal items
                    never_show_by_pattern = {},
                },
                follow_current_file = {
                    leave_dirs_open = false, -- `false` closes auto expanded dirs, such as with `:Neotree reveal`
                    enabled         = true,
                },
                hijack_netrw_behavior  = 'disabled',
                group_empty_dirs       = false, -- When true, empty folders will be grouped together
                use_libuv_file_watcher = true,  -- Uses the OS-level filewatchers to detect change instead of using nvim autocmds
                commands               = {
                    open_terminal = function (state)
                        local path    = state.tree:get_node().path
                        local sh_path = vim.fn.stdpath('config') .. '/scripts/term.sh'

                        vim.fn.jobstart(sh_path .. ' "' .. vim.fn.fnameescape(path) .. '"')
                    end,
                    grep_dir = function (state)
                        require('component.picker').search({ cwd = state.tree:get_node().path })
                    end,
                    find_files = function (state)
                        require('component.picker').files({ cwd = state.tree:get_node().path })
                    end,
                    upload_item = function (state)
                        local ok, rsync = pcall(require, 'simple-rsync')
                        if not ok or rsync == nil then
                            return
                        end

                        local item = state.tree:get_node().path
                        local cwd  = vim.fn.getcwd()

                        if string.sub(item, 1, #cwd) == cwd then
                            item = string.sub(item, #cwd + 1)
                        end

                        while string.sub(item, 1, 1) == '/' do
                            item = string.sub(item, 2)
                        end

                        if item ~= '' then
                            rsync.upload(item)
                        end
                    end,
                    download_item = function (state)
                        local ok, rsync = pcall(require, 'simple-rsync')
                        if not ok or rsync == nil then
                            return
                        end

                        local item = state.tree:get_node().path
                        local cwd  = vim.fn.getcwd()

                        if string.sub(item, 1, #cwd) == cwd then
                            item = string.sub(item, #cwd + 1)
                        end

                        while string.sub(item, 1, 1) == '/' do
                            item = string.sub(item, 2)
                        end

                        if item ~= '' then
                            rsync.download(item)
                        end
                    end,
                    delete_noconfirm = function(state, callback)
                        local node = state.tree:get_node()

                        require('neo-tree.sources.filesystem.lib.fs_actions').delete_node(
                            node.path,
                            callback,
                            true
                        )
                    end,
                },
                window = {
                    mappings = {
                        ['=']  = { 'show_help',     nowait = false, config = { title = 'Rsync', prefix_key = '=' }},
                        ['=d'] = { 'download_item', nowait = false, },
                        ['=u'] = { 'upload_item',   nowait = false, },
                        ['D']  = 'delete_noconfirm',
                        ['H']  = 'toggle_hidden',
                        ['t']  = 'open_terminal',
                        ['f']  = 'find_files',
                        ['/']  = 'grep_dir',
                    },
                },
            },
        })
    end,
}
