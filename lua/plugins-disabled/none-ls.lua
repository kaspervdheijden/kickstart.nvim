return {
    'nvimtools/none-ls.nvim',
    event = 'VeryLazy',
    cond  = function () return vim.fn.executable('./vendor/bin/php-cs-fixer') == 1 and vim.fn.executable('php') == 1 end,
    opts  = function ()
        local nls = require('null-ls')

        return {
            on_attach = function (client, buf)
                if vim.g.autoformat and client.supports_method('textDocument/formatting') then
                    require('component.configurator').autocmd({
                        'BufWritePre',
                        function ()
                            if vim.g.autoformat and vim.fn.executable('./vendor/bin/php-cs-fixer') then
                                vim.lsp.buf.format({ bufnr = buf, async = true })
                            end
                        end,
                        desc   = 'Format on save',
                        buffer = buf,
                    })
                end
            end,
            sources = {
                -- nls.builtins.diagnostics.phpstan.with({ command = './vendor/bin/phpstan', extra_args = { '--memory-limit=2G' } }),
                nls.builtins.formatting.phpcsfixer.with({ command = './vendor/bin/php-cs-fixer' }),
            },
        }
    end,
}
