return {
    'axkirillov/hbac.nvim',
    event        = { 'BufReadPost', 'BufNewFile' },
    opts         = { threshold = 6 },
    dependencies = {
        'nvim-telescope/telescope.nvim',
        'nvim-tree/nvim-web-devicons',
        'nvim-lua/plenary.nvim',
    },
}
