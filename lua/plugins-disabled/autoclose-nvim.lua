return {
    'https://gitlab.com/kaspervdheijden/autoclose.nvim.git',
    event = 'VeryLazy',
    opts  = {},
    keys  = {
        {
            '<leader>bu',
            function ()
                require('autoclose-nvim').run()
            end,
            desc = 'AutoClose run',
        },
    },
}
