return {
    'folke/todo-comments.nvim',
    event        = { 'BufReadPost', 'BufNewFile' },
    dependencies = { 'nvim-lua/plenary.nvim' },
    opts         = {
        highlight = {
            pattern = {
                [[.*\@<(KEYWORDS)\s+]],
                [[.*<(KEYWORDS)\s*:]],
            },
        },
    },
}
