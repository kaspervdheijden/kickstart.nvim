local colors = require('config.style').colors

return {
    'nvim-lualine/lualine.nvim',
    -- dependencies = { 'nvim-tree/nvim-web-devicons' },
    event        = 'VeryLazy',
    priority     = 1101,
    config       = function (_, opts)
        local lualine = require('lualine')

        require('component.configurator').autocmd({
            { 'RecordingEnter', 'RecordingLeave'},
            ---@diagnostic disable-next-line: undefined-field
            lualine.refresh,
            desc = 'Refresh lualine when start/stop recording',
        })

        ---@diagnostic disable-next-line: undefined-field
        lualine.setup(opts)
    end,
    opts = {
        options = {
            section_separators = (function (chrs)
                return {
                    left  = (chrs[1] or ''),
                    right = (chrs[2] or ''),
                }
            end)(vim.split(vim.env.NVIM_STATUS_SEP_CHARS or ':', ':')),
            disabled_filetypes   = { statusline = { 'dashboard', 'starter', 'ministarter' } },
            component_separators = { right = '•', left = '•' },
            theme                = 'auto',
            icons_enabled        = true,
            globalstatus         = true,
        },
        sections = {
            lualine_a = { 'mode' },
            lualine_b = { 'branch' },
            lualine_c = {
                (function (g_counter)
                    return {
                        function ()
                            local counter = 0
                            for _, buf in ipairs(vim.api.nvim_list_bufs()) do
                                if vim.bo[buf].buflisted then
                                    counter = counter + 1
                                end
                            end

                            g_counter = counter
                            return '[' .. counter .. ']'
                        end,
                        color = function ()
                            if g_counter < 4 then
                                return 'Character'
                            elseif g_counter < 7 then
                                return 'SpecialChar'
                            else
                                return 'Constant'
                            end
                        end,
                    }
                end)(0),
                {
                    'filename',
                    newfile_status = false,
                    file_status    = true,
                    separator      = '',
                    path           = 1,
                    symbols        = {
                        modified = '[+]',
                        readonly = '[-]',
                    },
                    padding = {
                        left  = 1,
                        right = 0,
                    },
                    color = function ()
                        if vim.bo.readonly then
                            return { fg = colors.highlight5 }
                        elseif vim.bo.modified then
                            return { fg = colors.light1 }
                        else
                            return nil
                        end
                    end,
                },
                {
                    'filetype',
                    icon_only = true,
                    separator = '',
                    padding   = {
                        left  = 1,
                        right = 0,
                    },
                },
                {
                    'b:fqclassname',
                    color     = { fg = colors.gray3 },
                    separator = '',
                    padding   = 0,
                },
                {
                    'diagnostics',
                    symbols = {
                        error = ' ',
                        warn  = ' ',
                        info  = ' ',
                        hint  = ' ',
                    },
                },
                -- {
                --     'diff',
                -- },
                {
                    'searchcount',
                    color    = { fg = colors.highlight4 },
                    maxcount = 999,
                    timeout  = 500,
                },
            },
            lualine_x = {
                {
                    function ()
                        local reg = vim.fn.reg_recording()

                        return reg == '' and '' or 'RECORDING to "' .. reg .. '"'
                    end,
                    color = { fg = colors.highlight1, bg = colors.highlight5, gui = 'italic,bold' },
                },
                {
                    function ()
                        return vim.g.hl_state == 1 and ' [h]' or ' '
                    end,
                    color     = { fg = colors.gray3 },
                    separator = '',
                },
                {
                    'g:version',
                    color     = { fg = colors.gray3 },
                    separator = '',
                    padding   = {
                        left  = 0,
                        right = 1,
                    },
                },
                {
                    require('component.util').get_updates,
                    color = { fg = colors.highlight2 },
                },
                {
                    function ()
                        return vim.o.mouse == '' and '' or 'mouse'
                    end,
                    color     = { fg = colors.highlight1 },
                    separator = '',
                    padding   = {
                        left  = 1,
                        right = 1,
                    }
                },
                {
                   function ()
                        return '[' .. table.concat(require('component.lsp').get_client_names(0), ', ') .. ']'
                    end,
                    color     = { fg = colors.gray2 },
                    separator = '',
                },
                {
                    'encoding',
                    show_bomb = true,
                    separator = '',
                    padding   = 1,
                    fmt       = function (text)
                        return text == '' and '?' or text
                    end,
                },
                {
                    'fileformat',
                    separator = '',
                    padding   = {
                        left  = 0,
                        right = 2,
                    },
                },
            },
            lualine_y = { 'progress' },
            lualine_z = { 'location' },
        },
    },
}
