return {
    'akinsho/bufferline.nvim',
    event = 'VeryLazy',
    keys  = {
        { '<C-PageUp>',   '<cmd>BufferLineCyclePrev<CR>',   desc = 'Previous buffer', mode = { 'n', 'i', 'v' } },
        { '<C-PageDown>', '<cmd>BufferLineCycleNext<CR>',   desc = 'Next buffer',     mode = { 'n', 'i', 'v' } },
        { '<C-Ins>',      '<cmd>BufferLinePick<CR>',        desc = 'Choose buffer',   mode = { 'n', 'i', 'v' } },
        { '<leader>br',   '<Cmd>BufferLineCloseRight<CR>',  desc = 'Delete buffers to the right' },
        { '<leader>bl',   '<Cmd>BufferLineCloseLeft<CR>',   desc = 'Delete buffers to the left'  },
        { '<leader>bo',   '<cmd>BufferLineCloseOthers<CR>', desc = 'Close other buffers'         },
    },
    opts = {
        options = {
            diagnostics            = 'nvim_lsp',
            truncate_names         = false,
            always_show_bufferline = true,
            style_preset           = 2, -- { 1=default, 2=minimal, 3=no_bold, 4=no_italic },
            close_command          = function (buf)
                local ok, mbr = pcall(require, 'mini.bufremove')

                if ok and mbr ~= nil then
                    mbr.delete(buf, false)
                else
                    vim.api.nvim_buf_delete(buf, {})
                end
            end,
            diagnostics_indicator = function (_, _, diag)
                local indicator = ''
                local icons     = {
                    error = ' ',
                    warn  = ' ',
                   -- hint  = ' ',
                   -- info  = ' ',
                }

                for kind, icon in pairs(icons) do
                    if diag[kind] then
                        indicator = indicator .. icon
                    end
                end

                return indicator
            end,
            tab_size = 24,
            offsets  = {
                {
                    filetype   = 'neo-tree',
                    highlight  = 'Comment',
                    text_align = 'left',
                    separator  = false,
                    text       = function ()
                        return string.format('󰉓  %s', vim.fn.fnamemodify(vim.fn.getcwd(), ':t'))
                    end,
                },
            },
        },
    },
    config = function (_, opts)
        require('bufferline').setup(opts)

        local cfg = require('component.configurator')

        cfg.register_key_groups({ { '<leader>b', 'Buffers' } })
        -- Fix bufferline when restoring a session
        cfg.autocmd({
            'BufAdd',
            function ()
                ---@diagnostic disable-next-line: undefined-global
                vim.schedule(function () pcall(nvim_bufferline) end)
            end,
        })
    end,
}
