return {
    'ThePrimeagen/harpoon',
    requires = { 'nvim-lua/plenary.nvim' },
    branch   = 'harpoon2',
    event    = 'VeryLazy',
    keys     = function ()
        local harpoon
        local hp = function ()
            harpoon = harpoon or require('harpoon');

            return harpoon
        end

        return {
            { '<leader>ha', function () hp():list():append()                   end, desc = 'Harpoon add'      },
            { '<leader>h1', function () hp():list():select(1)                  end, desc = 'Harpoon jump 1'   },
            { '<leader>h2', function () hp():list():select(2)                  end, desc = 'Harpoon jump 2'   },
            { '<leader>h3', function () hp():list():select(3)                  end, desc = 'Harpoon jump 3'   },
            { '<leader>h4', function () hp():list():select(4)                  end, desc = 'Harpoon jump 4'   },
            { '<leader>hn', function () hp():list():next()                     end, desc = 'Harpoon next'     },
            { '<leader>hp', function () hp():list():prev()                     end, desc = 'Harpoon previous' },
            { '<leader>hl', function () hp().ui:toggle_quick_menu(hp():list()) end, desc = 'Harpoon list',    },
        }
    end,
    config = function ()
        require('harpoon').setup()
        require('component.configurator').register_key_groups({ { '<leader>h', 'Harpoon' } })
    end,
}
