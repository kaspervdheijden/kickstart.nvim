local queue = {}

return {
    add = function (queueName, item)
        if item == nil or item == '' then
            return
        end

        if queue[queueName] == nil then
            queue[queueName] = {}
        end

        for i, name in ipairs(queue[queueName]) do
            if name == item then
                table.remove(queue[queueName], i)
            end
        end

        table.insert(queue[queueName], 1, item)
    end,

    pop = function (queueName)
        if queue[queueName] == nil or #queue[queueName] == 0 then
            return nil
        end

        return table.remove(queue[queueName], 1)
    end,
}
