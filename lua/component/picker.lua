return (function (implementor)
    local impl = require('component.pickers.' .. implementor)

    return {
        search_word = impl.search_word,
        buffers     = impl.buffers,
        resume      = impl.resume,
        implementor = implementor,
        spell       = impl.spell,
        lsp         = impl.lsp,

        files = function (opts)
            impl.files(opts or {})
        end,

        search = function (opts)
            impl.search(opts or {})
        end,
    }
end)('snacks')
