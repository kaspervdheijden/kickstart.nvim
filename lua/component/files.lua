return (function (picker, explorer)
    return {
        explorer   = require('component.explorers.' .. explorer).open,
        find_files = picker.files,
        grep       = function ()
            local cur_dir  = vim.fn.getcwd()
            local root_dir = vim.fn.systemlist('git -C ' .. vim.fn.escape(cur_dir, ' ') .. ' rev-parse --show-toplevel')[1]

            if vim.v.shell_error ~= 0 then
                root_dir = cur_dir
            end

            picker.search({ search_paths = { root_dir }})
        end,
    }
end)(require('component.picker'), 'snacks')
