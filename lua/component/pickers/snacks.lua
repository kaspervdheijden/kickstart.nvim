
local function snack()
    require('nvim-treesitter')

    return require('snacks')
end

return {
    buffers = function ()
        snack().picker.buffers()
    end,

    resume = function ()
        snack().picker.resume()
    end,

    keymaps = function ()
        snack().picker.keymaps()
    end,

    search = function (opts)
        snack().picker.grep({ dirs = { (opts or {}).path or vim.fn.getcwd() } })
    end,

    search_word = function (opts)
        snack().picker.grep_word({ dirs = { (opts or {}).path or vim.fn.getcwd() } })
    end,

    files = function ()
        snack().picker.smart({
            filter = { cwd = true },
            title  = 'Files',
        })
    end,

    lsp = {
        project_symbols  = function () snack().picker.lsp_workspace_symbols() end,
        document_symbols = function () snack().picker.lsp_symbols()           end,
        type_definitions = function () snack().picker.lsp_type_definitions()  end,
        implementations  = function () snack().picker.lsp_implementations()   end,
        references       = function () snack().picker.lsp_references()        end,
        definitions      = function () snack().picker.lsp_definitions()       end,
        diagnostics      = function () snack().picker.diagnostics_buffer()    end,
        quickfix         = function () snack().picker.qflist()                end,
    },
}
