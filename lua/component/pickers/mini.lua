return (function (lsp_action)
    return {
        buffers = function ()
            MiniPick.builtin.buffers({ include_current = false })
        end,

        resume = function ()
            MiniPick.builtin.resume()
        end,

        keymaps = function ()
            MiniExtra.pickers.keymaps()
        end,

        search = function (opts)
            local options = opts or {}

            MiniPick.builtin.grep_live({}, { source = { cwd = options.path } })
        end,

        search_word = function (opts)
            local options = opts or {}

            MiniPick.builtin.grep_live({}, { source = { cwd = options.path } })
        end,

        files = function (opts)
            local cwd     = opts.path or vim.fn.getcwd()
            local len     = cwd:len() + 1
            local recents = vim.tbl_map(
                function (path)
                    if not vim.startswith(path, cwd) then
                        return vim.fn.fnamemodify(path, ':~')
                    end

                    return path:sub(len):gsub('^/+', ''):gsub('/+$', '')
                end,
                MiniVisits.list_paths(cwd)
            )

            MiniPick.builtin.cli({
                command     = opts.command or { 'rg', '--files', '--no-follow', '--color=never', '--sortr=modified', '--no-heading', '--column', '--trim', },
                postprocess = function (items)
                    local result = {}

                    for _, item in ipairs(recents) do
                        if item ~= '' then
                            table.insert(result, item)
                        end
                    end

                    for _, item in ipairs(items) do
                        if item ~= '' and not vim.tbl_contains(result, item) then
                            table.insert(result, item)
                        end
                    end

                    return result
                end,
            }, {
                source = {
                    choose = vim.schedule_wrap(MiniPick.default_choose),
                    name   = '',
                    show   = function (buf_id, items, query)
                        MiniPick.default_show(buf_id, items, query, { show_icons = true })
                    end,
                },
            })
        end,

        lsp = {
            project_symbols  = lsp_action('workspace_symbol'),
            document_symbols = lsp_action('document_symbol'),
            type_definitions = lsp_action('type_definition'),
            implementations  = lsp_action('implementation'),
            references       = lsp_action('references'),
            definitions      = lsp_action('definition'),

            diagnostics = function ()
                MiniExtra.pickers.diagnostic({ scope = 'curent' })
            end,

            quickfix = function ()
                vim.api.nvim_command('copen')
            end,
        },
    }
end)(function (methodName)
    return function ()
        MiniExtra.pickers.lsp({ scope = methodName })
    end
end)
