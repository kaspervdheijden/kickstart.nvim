return (function (lsp)
    return {
        buffers = function ()
            require('telescope.builtin').buffers({
                ignore_current_buffer = true,
                sort_lastused         = true,
                sort_mru              = true,
            })
        end,

        resume = function ()
            require('telescope.builtin').resume()
        end,

        keymaps = function ()
            require('telescope.builtin').keymaps()
        end,

        search = function (opts)
            require('telescope.builtin').live_grep({
                search_paths = opts.search_paths,
                cwd          = opts.path,
            })
        end,

        search_word = function (opts)
            require('telescope.builtin').grep_string({
                search_paths = opts.search_paths,
                cwd          = opts.path,
            })
        end,

        files = function (opts)
            local telescope = require('telescope')
            local options   = {
                hidden = (opts or {}).hidden,
                cwd    = (opts or {}).path,
            }

            if options.cwd ~= nil then
                require('telescope.builtin').find_files(options)
            elseif telescope.extensions['recent-files'] ~= nil then
                telescope.extensions['recent-files'].recent_files(options)
            elseif telescope.extensions['frecency'] ~= nil then
                telescope.extensions['frecency'].frecency(options)
            else
                require('telescope.builtin').find_files(options)
            end
        end,

        lsp = {
            project_symbols  = lsp.action('workspace_symbol'),
            document_symbols = lsp.action('document_symbol'),
            type_definitions = lsp.action('type_definition'),
            implementations  = lsp.action('implementation'),
            references       = lsp.action('references'),
            definitions      = lsp.action('definition'),

            diagnostics = function ()
                require('telescope.builtin').diagnostics()
            end,

            quickfix = function ()
                require('telescope.builtin').quickfix()
            end,
        },
    }
end)({
    action = function (methodName)
        local mapping = {
            workspace_symbol = 'lsp_dynamic_workspace_symbols',
            document_symbol  = 'lsp_document_symbols',
            type_definition  = 'lsp_type_definitions',
            implementation   = 'lsp_implementations',
            definition       = 'lsp_definitions',
            references       = 'lsp_references',
        }

        return function ()
            local method = mapping[methodName]
            if method ~= nil then
                return require('telescope.builtin')[method]()
            end

            return vim.lsp.buf[methodName]()
        end
    end,
})
