local package_manager = {
    updates = require('lazy.status').updates,
    stats   = require('lazy').stats,
}

local state = {
    win = nil,
    buf = nil,
}

local H = {
    float_term = function (cmd, buf, opts)
        opts          = opts or {}
        opts.width    = opts.width  or math.floor(vim.o.columns * 0.8)
        opts.height   = opts.height or math.floor(vim.o.lines * 0.75)
        opts.col      = opts.col    or math.floor((vim.o.columns - opts.width) / 2)
        opts.row      = opts.row    or math.floor((vim.o.lines - opts.height) / 2)
        opts.border   = opts.border or require('config.style').border
        opts.style    = 'minimal'
        opts.relative = 'editor'

        if buf == nil or not vim.api.nvim_buf_is_valid(buf) then
            buf = vim.api.nvim_create_buf(false, true)
        end

        local win = vim.api.nvim_open_win(buf, true, opts)
        if vim.bo[buf].buftype ~= 'terminal' then
            vim.fn.termopen(cmd)
        end

        vim.api.nvim_command('startinsert')

        local cfg = require('component.configurator')

        cfg.keymap({
            { 'n', 'v', 's' },
            '<C-c>',
            '<ESC>A',
            desc   = 'Go to insert mode',
            remap  = false,
            silent = true,
            buffer = buf,
        })

        cfg.autocmd(
            {
                'TermClose',
                function ()
                    pcall(vim.api.nvim_win_close, win, true)
                end,
                desc = 'Close window after closing the terminal',
            },
            'on-term-close'
        )

        return { win = win, buf = buf }
    end,

    notifier = function (level)
        return function (msg)
            local notify = function ()
                vim.api.nvim_notify(msg, level or vim.log.levels.INFO, {})
            end

            if vim.in_fast_event() then
                vim.schedule(notify)
            else
                notify()
            end
        end
    end,
}

local notify = {
    err  = H.notifier(vim.log.levels.ERROR),
    warn = H.notifier(vim.log.levels.WARN),
    info = H.notifier(vim.log.levels.INFO),
}

return {
    stats       = package_manager.stats,
    float_term  = H.float_term,
    notify      = notify,

    toggle_float_term = function ()
        if state.win ~= nil and vim.api.nvim_win_is_valid(state.win) then
            vim.api.nvim_win_hide(state.win)
        else
            state = H.float_term({ vim.o.shell or 'bash' }, state.buf)
        end
    end,

    copy_and_notify = function (data_to_copy, description, register)
        if (data_to_copy or '') == '' then
            return
        end

        vim.fn.setreg(register or '+', data_to_copy)
        if (description or '') == '' then
            notify.info(data_to_copy)
        else
            notify.info(string.format('Copied %s "%s"', description, data_to_copy))
        end
    end,

    get_updates = function ()
        return (package_manager.updates() or '')
    end,
}
