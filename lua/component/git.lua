return (function (util)
    local H = {
        wrapper     = string.format('%s/scripts/git-wrapper.sh', vim.fn.stdpath('config')),
        ensure_file = function (opts)
            local filename = (opts or {}).file or vim.fn.expand('%:.') or ''

            if filename == '' then
                util.notify.err('No filename given')
            end

            return filename
        end,

        run = function (cmd, interactive)
            if interactive then
                util.float_term(cmd)
            else
                vim.system(
                    cmd,
                    { text = true },
                    vim.schedule_wrap(function (result)
                        if result.code == 0 then
                            vim.api.nvim_command('checktime')
                        end
                    end)
                )
            end
        end,

        interactive = function (opts)
            return (opts or {}).interactive
        end,
    }

    return {
        add = function (opts)
            local filename = H.ensure_file(opts)

            if filename ~= '' then
                H.run(
                    { H.wrapper, (H.interactive(opts) and 'add-interactive' or 'add'), filename },
                    H.interactive(opts)
                )
            end
        end,

        unstage = function (opts)
            local filename = H.ensure_file(opts)

            if filename ~= '' then
                H.run(
                    { H.wrapper, H.interactive(opts) and 'restore-interactive' or 'restore', filename },
                    H.interactive(opts)

                )
            end
        end,

        checkout = function (opts)
            local filename = H.ensure_file(opts)

            if filename ~= '' then
                H.run({ H.wrapper, 'checkout', filename }, false)
            end
        end,

        switch_branch = function ()
            H.run({ H.wrapper, 'switch-branch' }, false)
        end,

        remote_url = function ()
            vim.system(
                { H.wrapper, 'remote-url' },
                { text = true },
                vim.schedule_wrap(function (output)
                    util.copy_and_notify(output.stdout, '')

                    if vim.env.DESKTOP_SESSION ~= nil then
                        vim.ui.open(output.stdout)
                    end
                end)
            )
        end,

        update_repo = function ()
            H.run({ H.wrapper, 'update', (vim.o.shell or 'bash') }, true)
        end,

        branch_name = function (format, try_harder)
            local branch = ((vim.b.minigit_summary or {}).head_name or vim.b.gitsigns_head) or ''

            if branch == '' and try_harder then
                branch = vim.fn.system({ H.wrapper, 'branch-name'})

                if branch == '' then
                    return ''
                end
            end

            return string.format(format or '%s', branch)
        end,
    }
end)(require('component.util'))
