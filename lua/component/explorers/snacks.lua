return {
    open = function ()
        require('snacks').picker.explorer({
            include = { 'vendor' },
            hidden  = true,
            actions = {
                localsearch = function (_, item)
                    while item ~= nil and not item.dir do
                        item = item.parent
                    end

                    if item ~= nil then
                        require('component.picker').search({ path = item.file })
                    end
                end,
            },
            win = {
                list = {
                    keys = {
                        ['<C-s>'] = { 'edit_vsplit', mode = { 'i', 'n' } },
                        ['<C-v>'] = { 'edit_split',  mode = { 'i', 'n' } },
                        ['/']     = 'localsearch',
                        ['<C-c>'] = 'nop',
                        ['<ESC>'] = 'nop',
                    },
                },
            },
            on_show = function ()
                require('component.configurator').autocmd({
                    'ModeChanged',
                    function ()
                        vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<ESC>', true, false, true), 'x', true)
                    end,
                    desc    = 'Disable visualmode for explorer',
                    buffer  = vim.api.nvim_get_current_buf(),
                })
            end,
        })
    end,
}
