return {
    open = function ()
        local files = require('mini.files')

        if files.close() then
            return
        end

        local name = vim.api.nvim_buf_get_name(0)
        local path = nil

        if vim.fn.isdirectory(name) == 1 or vim.fn.filereadable(name) == 1 then
            path = name
        end

        files.open(path, false)
    end,
}
