return (function (util, git, files, conf, conf_dir, dir)
    return {
        headers = function (skip_blank_lines)
            local file     = vim.env.NVIM_HEADER_FILE or ''
            local vars     = {
                DIRNAME = vim.fn.fnamemodify(dir, ':t'),
                USER    = vim.env.USER or '',
                VERSION = vim.g.version,
                DIR     = dir,
            }

            if file ~= '' and vim.fn.filereadable(file) == 0 then
                file = conf_dir .. '/headers/' .. file

                if vim.fn.filereadable(file) == 0 then
                    file = ''
                end
            end

            if file == '' then
                file = conf_dir .. '/headers/default.header'
            end

            local headers = {}
            for line in io.lines(file) do
                local item = line

                if item ~= '' then
                    for name, val in pairs(vars) do
                        item = string.gsub(item, '%$' .. name .. '%$', val)
                    end
                end

                if skip_blank_lines ~= true or item ~= '' then
                    table.insert(headers, item)
                end
            end

            return headers
        end,

        footer = function ()
            local stats       = util.stats()
            local startuptime = math.floor(stats.startuptime * 100 + 0.5) / 100
            local dir_name    = vim.fn.fnamemodify(dir, ':t')
            local branch      = git.branch_name('%s', true)
            local icons       = ''

            for threshold = 14, 54, 9 do
                if startuptime < threshold then
                    icons = icons .. '💜'
                end
            end

            return {
                '',
                string.format(
                    '%s Loaded %s/%s plugins in %s ms %s',
                    icons,
                    stats.loaded,
                    stats.count,
                    startuptime,
                    icons
                ),
                branch ~= '' and dir_name  .. ' @ ' ..  branch or dir_name,
            }
        end,

        items = function (refresh)
            return {
                { 'Recent file',            files.find_files,       'p' },
                { 'Search text',            files.grep,             '/' },
                { 'File explorer',          files.explorer,         'g' },
                -- { },
                { 'Lazy',                   'Lazy',                 'l' },
                { 'Lazy sync',              'Lazy sync',            's' },
                { 'Mason',                  'Mason',                'm' },
                -- { },
                { 'Switch branch',          git.switch_branch,      'b' },
                { 'Terminal',               util.toggle_float_term, 't' },
                { 'Update repo',            git.update_repo,        'u' },
                { 'Browse repo',            git.remote_url,         'o' },
                -- { },
                { 'Health check',           'checkhealth',          'h' },
                { 'Changelog',              'help news',            'c' },
                -- { },
                { 'New file',               'enew | startinsert',   'n' },
                { 'Refresh',                refresh,                'r' },
                { 'Quit',                   'qa!',                  'q' },
            }
        end,

        opener = function (callback)
            local closer = require('component.closer')

            return function ()
                closer.closeAll()
                callback()
            end
        end,

        set_handlers_to_lauch_after_last_buffer_close = function (greeter_buf_filetype, callback)
            conf.autocmd({
                'BufDelete',
                function (event)
                    if not vim.bo.buflisted then
                        return
                    end

                    local ft = vim.bo.filetype
                    for _, tpe in ipairs({ greeter_buf_filetype, 'snacks_picker_list', 'telescopeprompt', 'checkhealth', 'lazy', 'mason', 'man', 'help', 'dashboard', 'lspinfo', 'netrw', 'oil' }) do
                        if tpe == ft then
                            return
                        end
                    end

                    for buf = 1, vim.fn.bufnr('$') do
                        if buf ~= event.buf and vim.fn.buflisted(buf) == 1 then
                            if vim.api.nvim_buf_get_name(buf) ~= '' and vim.bo[buf].filetype ~= greeter_buf_filetype then
                                return
                            end
                        end
                    end

                    callback()
                end,
                desc = 'Open dashboard after closing last buffer',
            })
        end,
    }
end)(
    require('component.util'),
    require('component.git'),
    require('component.files'),
    require('component.configurator'),
    vim.fn.stdpath('config'),
    vim.fn.getcwd()
)
