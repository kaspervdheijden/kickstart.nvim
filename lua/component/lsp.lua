local get_lsps = function (buf)
    local opts = { bufnr = buf, buffer = buf }

    ---@diagnostic disable-next-line: deprecated
    local lsps = vim.lsp.get_clients and vim.lsp.get_clients(opts) or vim.lsp.get_active_clients(opts)
    local ret  = {}

    for _, lsp in ipairs(lsps) do
        if lsp and not lsp.is_stopped() and lsp.initialized then
            ret[#ret + 1] = lsp
        end
    end

    return ret
end

local picker = require('component.picker')

return {
    diagnostics_next = function () vim.diagnostic.goto_next({ float = false }) end,
    diagnostics_prev = function () vim.diagnostic.goto_prev({ float = false }) end,
    document_symbols = picker.lsp.document_symbols,
    type_definitions = picker.lsp.type_definitions,
    implementations  = picker.lsp.implementations,
    project_symbols  = picker.lsp.project_symbols,
    definitions      = picker.lsp.definitions,
    diagnostics      = picker.lsp.diagnostics,
    references       = picker.lsp.references,
    quickfix         = picker.lsp.quickfix,
    get_client_names = function (buf)
        local names = {}

        for _, lsp in ipairs(get_lsps(buf)) do
            names[lsp.name] = lsp.name
        end

        table.sort(names)
        return vim.tbl_values(names)
    end,

    servers          = function ()
        ---@diagnostic disable-next-line: param-type-mismatch
        local share_dir = vim.fn.fnamemodify(vim.fn.stdpath('state'), ':p:h:h')
        return {
            html   = { filetypes = { 'hbs', 'htm', 'html' } },
            yamlls = { filetypes = { 'yaml' } },
            cssls  = { filetypes = { 'css' } },
            bashls = {},
            jsonls = {
                init_options = { provideFormatter = false },
                filetypes    = { 'json' },
            },
            lemminx = {
                filetypes = { 'uxf', 'xml', 'xslt', 'xsl', 'svg', },
                xml       = {
                    server = { workDir = share_dir .. '/lemminx' },
                },
            },
            lua_ls  = {
                Lua = {
                    diagnostics = { disable = { 'missing-fields' } },
                    runtime     = { version = 'LuaJIT' },
                    workspace   = {
                        checkThirdParty = false,
                        library         = {
                            vim.env.VIMRUNTIME,
                            '${3rd}/luv/library',
                            unpack(vim.api.nvim_get_runtime_file('', true)),
                        },
                    },
                },
            },
            intelephense = {
                init_options = {
                    licenceKey        = vim.env.NVIM_LSP_INTELEPHENSE_LICENCEKEY,
                    exclude           = { 'vendor/**/.phpstan/stubs/**' },
                    globalStoragePath = share_dir .. '/intelephense',
                    files             = {},
                },
            },
        }
    end,

    capabilities = function ()
        local capabilities = vim.lsp.protocol.make_client_capabilities()

        local success, cmp = pcall(require, 'cmp_nvim_lsp')
        if success and cmp ~= nil then
            return cmp.default_capabilities(capabilities)
        end

        success, cmp = pcall(require, 'blink.cmp')
        if success and cmp ~= nil then
            return cmp.get_lsp_capabilities(capabilities)
        end

        return capabilities
    end,

    restart = function ()
        local names = {}

        for _, lsp in ipairs(get_lsps(0)) do
            vim.api.nvim_command('LspRestart ' .. lsp.id)
            names[#names + 1] = lsp.name
        end

        require('component.util').notify.warn(
            #names == 0 and 'No clients found' or string.format(
                'LSP "%s" restarted',
                table.concat(names, '", "')
            )
        )
    end,
}
