local H = {
    keymap = function (entry)
        if entry == nil or entry[1] == nil or entry[2] == nil or entry[3] == nil then
            return
        end

        if entry.delete then
            vim.keymap.del(entry[1], entry[2])
        else
            vim.keymap.set(entry[1], entry[2], entry[3], {
                desc    = entry.desc    or ('No description provided for "' .. entry[2] .. '". Consider adding a description.'),
                noremap = entry.noremap or false,
                silent  = entry.silent  or false,
                expr    = entry.expr    or false,
                buffer  = entry.buffer  or nil,
            })
        end
    end,

    autocmd = function (entry, group)
        if entry == nil then
            return
        end

        if type(group) == 'string' then
            group = vim.api.nvim_create_augroup(group, { clear = true }) or nil
        end

        vim.api.nvim_create_autocmd(
            entry[1],
            {
                callback = entry[2]      or function () end,
                group    = entry.group   or group or nil,
                once     = entry.once    or false,
                pattern  = entry.pattern or nil,
                buffer   = entry.buffer  or nil,
                desc     = entry.desc    or nil,
            }
        )
    end,

    set_hl = function (entry)
        if entry == nil or entry[1] == nil then
            return
        end

        local hl = {
            strikethrough = entry.strikethrough or nil,
            underdouble   = entry.underdouble or nil,
            underdotted   = entry.underdotted or nil,
            underdashed   = entry.underdashed or nil,
            undercurl     = entry.undercurl or nil,
            underline     = entry.underline or nil,
            nocombine     = entry.nocombine or nil,
            standout      = entry.standout or nil,
            reverse       = entry.reverse or nil,
            default       = entry.default or nil,
            italic        = entry.italic or nil,
            force         = entry.force or nil,
            blend         = entry.blend or nil,
            bold          = entry.bold or nil,
            link          = entry.link or nil,
            bg            = entry.bg or nil,
            fg            = entry.fg or nil,
            sp            = entry.sp or nil,
        }

        if entry.delete then
            hl = {}
        elseif entry.from then
            hl = vim.api.nvim_get_hl(0, { name = entry.from }) or {}

            for key, val in pairs(entry) do
                if key ~= 'from' and key ~= 1 then
                    if type(val) == 'function' then
                        hl[key] = val()
                    else
                        hl[key] = val
                    end
                end
            end
        end

        ---@diagnostic disable-next-line: param-type-mismatch
        vim.api.nvim_set_hl(0, entry[1], hl)
    end,
}

return {
    keymap  = H.keymap,
    autocmd = H.autocmd,
    set_hl  = H.set_hl,

    options = function (options)
        for type, opts in pairs(options) do
            for key, value in pairs(opts) do
                vim[type][key] = value
            end
        end
    end,

    set_hls = function (hls)
        for _, entry in ipairs(hls) do
            H.set_hl(entry)
        end
    end,

    keymaps = function (keys)
        for _, entry in ipairs(keys) do
            H.keymap(entry)
        end
    end,

    autocmds = function (autocmds, groupname)
        local group = groupname and vim.api.nvim_create_augroup(groupname, { clear = true }) or nil

        for _, entry in ipairs(autocmds) do
            H.autocmd(entry, group)
        end
    end,

    register_key_groups = function (items)
        local mc, miniclue = pcall(require, 'mini.clue')
        if mc and miniclue ~= nil then
            for _, item in ipairs(items) do
                table.insert(MiniClue.config.clues, {
                    mode = item.mode or 'n',
                    keys = item[1],
                    desc = item[2],
                })
            end
        end

        local wk, which_key = pcall(require, 'which-key')
        if wk and which_key ~= nil then
            for _, item in ipairs(items) do
                local element = {
                    {
                        item[1],
                        mode  = item.mode or 'n',
                        group = item[2],
                    },
                }

                if item.hidden ~= false then
                    table.insert(element, {
                        item[1] .. '_',
                        mode   = element[1].mode,
                        hidden = true,
                    })
                end

                which_key.add(element)
            end
        end
    end,
}
