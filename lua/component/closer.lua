return (function (closer)
    return {
        close    = closer,
        closeAll = function (opts)
            local options = opts or {}

            for _, buf in ipairs(vim.api.nvim_list_bufs()) do
                if
                    buf ~= options.bufToSkip        and
                    vim.api.nvim_buf_is_loaded(buf) and
                    vim.api.nvim_buf_is_valid(buf)  and
                    vim.bo[buf].buflisted
                then
                    local ok, result = pcall(closer, { buf = buf, switch = false, })
                    if ok and result == 0 then
                        return
                    end
                end
            end
        end,
    }
end)(function (opts)
    local options  = opts or {}
    local buf      = options.buf or vim.api.nvim_get_current_buf()
    local modified = vim.bo[buf].modified

    if options.switch ~= false then
        local alt_buf  = vim.fn.bufnr('#')

        if alt_buf ~= buf and vim.fn.buflisted(alt_buf) == 1 then
            pcall(vim.api.nvim_win_set_buf, 0, alt_buf)
        else
            ---@diagnostic disable-next-line: param-type-mismatch
            pcall(vim.api.nvim_command, 'silent bprevious')
        end
    end

    if not modified or options.force then
        vim.api.nvim_command('silent bdelete! ' .. buf)
        return
    end

    local choice = vim.fn.confirm(
        string.format(
            'Buffer modified. Save "%q" before closing?',
            vim.fn.fnamemodify(vim.api.nvim_buf_get_name(buf) ':.')
        ),
        '&Yes\n&No\n&Cancel',
        3,
        'Question'
    )

    if choice == 1 then
        vim.api.nvim_command('write ++p')
    end

    if choice == 1 or choice == 2 then
        vim.api.nvim_command('silent bdelete ' .. buf)
    end

    return choice
end)
