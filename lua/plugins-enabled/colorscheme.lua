return (function (repo, plugin_name, name, opts)
    return {
        repo,
        cond     = (vim.env.NVIM_COLORSCHEME or '') == '',
        name     = plugin_name,
        event    = 'UIEnter',
        priority = 1000,
        config = function ()
            require(name).setup(opts)

            vim.api.nvim_command('colorscheme ' .. name)
        end,
    }
end)(
    'catppuccin/nvim',
    'catppuccin.nvim',
    'catppuccin',
    {
        integrations = {
            blink_cmp = true,
            gitsigns  = true,
            mason     = true,
            snacks    = true,
        },
        transparent_background = true,
        custom_highlights      = function (colors)
            return {
                netrwTreeBar = { fg = colors.surface0 },
            }
        end,
    }
)
