return {
    'nvim-treesitter/nvim-treesitter',
    -- dependencies = {
    --     'nvim-treesitter/nvim-treesitter-textobjects',
    --     'nvim-treesitter/nvim-treesitter-context',
    --     'nvim-treesitter/playground',
    -- },
    event = { 'BufNewFile', 'BufReadPost' },
    main  = 'nvim-treesitter.configs',
    build = ':TSUpdate',
    opts  = {
        auto_install     = true,
        ensure_installed = {
            'markdown_inline', 'javascript', 'markdown', 'vimdoc',
            'regex',           'bash',       'html',     'java',
            'json',            'lua',        'php',      'sql',
            'twig',            'yaml',       'vim',
        },
        indent = {
            disabled = { 'sql' },
            enable   = true,
        },
        highlight = {
            enable  = true,
            disable = function (_, buf)
                local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))

                return ok and stats and stats.size > 256000 --250kb
            end,
        },
        incremental_selection = {
            enable  = true,
            keymaps = {
                node_incremental  = '<C-Space>',
                node_decremental  = '<M-Space>',
                init_selection    = '<C-Space>',
            },
        },
        playground = {
            enable          = true,
            disable         = {},
            updatetime      = 25,    -- Debounced time for highlighting nodes in the playground from source code
            persist_queries = false, -- Whether the query persists across vim sessions
            keybindings     = {
                toggle_query_editor       = 'o',
                toggle_hl_groups          = 'i',
                toggle_injected_languages = 't',
                toggle_anonymous_nodes    = 'a',
                toggle_language_display   = 'I',
                focus_language            = 'f',
                unfocus_language          = 'F',
                update                    = 'R',
                goto_node                 = '<CR>',
                show_help                 = '?',
            },
        },
        textobjects = {
            select = {
                enable    = true,
                lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
                keymaps   = {
                    -- You can use the capture groups defined in textobjects.scm
                    ['aa'] = '@parameter.outer',
                    ['ia'] = '@parameter.inner',
                    ['af'] = '@function.outer',
                    ['if'] = '@function.inner',
                    ['ac'] = '@class.outer',
                    ['ic'] = '@class.inner',
                },
            },
            move = {
                enable          = true,
                set_jumps       = true, -- whether to set jumps in the jumplist
                goto_next_start = {
                    [']m'] = '@function.outer',
                    [']]'] = '@class.outer',
                },
                goto_previous_start = {
                    ['[m'] = '@function.outer',
                    ['[['] = '@class.outer',
                },
            },
            -- swap = {
            --     enable    = true,
            --     swap_next = {
            --         ['<leader>cn'] = '@parameter.inner',
            --     },
            --     swap_previous = {
            --         ['<leader>cp'] = '@parameter.inner',
            --     },
            -- },
        },
    },
}
