return (function (components)
    return {
        'echasnovski/mini.nvim',
        event  = 'VeryLazy',
        config = function ()
            local M = {
                cfg   = require('component.configurator'),
                files = require('component.files'),
                util  = require('component.util'),
                lsp   = require('component.lsp'),
                style = require('config.style'),
            }

            for key, item in pairs(components) do
                local component

                if type(item) == 'function' then
                    component = require('mini.' .. key)
                    item      = item(component, M)
                end

                if item.enabled ~= false then
                    if component == nil then
                        component = require('mini.' .. key)
                    end

                    component.setup(item.opts or {})

                    M.cfg.autocmds(item.autocmds or {})
                    M.cfg.keymaps(item.keymaps or {})
                    M.cfg.set_hls(item.hls or {})

                    if type(item.init) == 'function' then
                        item.init(component, M)
                    end
                end
            end
        end,
    }
end)({
    splitjoin = function (splitjoin)
        local parens = { brackets = { '%b()' } }

        return {
            opts = {
                split = { hooks_post = { splitjoin.gen_hook.add_trailing_separator(parens) } },
                join  = { hooks_post = { splitjoin.gen_hook.del_trailing_separator(parens) } },
            },
            keymaps = {
                { 'n', '<leader>cj', splitjoin.toggle, desc = 'Toggle arguments' },
            },
        }
    end,

    trailspace = function (trailspace, M)
        return {
            hls  = {
                { 'MiniTrailspace', bg = 'brown' },
            },
            autocmds = {
                {
                    'FileType',
                    function (event)
                        M.cfg.autocmd({
                            'BufWritePre',
                            function ()
                                if not vim.b.notrimonsave then
                                    trailspace.trim()
                                end
                            end,
                            desc   = 'Trim whitespace before saving',
                            buffer = event.buf,
                            silent = true,
                        })
                    end,
                    pattern = {
                        'javascript', 'python', 'bash', 'make',
                        'perl',       'rust',   'cpp',  'lua',
                        'php',        'sql',    'go',   'c',
                    },
                },
            },
            keymaps = {
                {
                    'n',
                    '<C-a-s>',
                    function ()
                        vim.b.notrimonsave = true
                        vim.api.nvim_command('write ++p')
                        vim.b.notrimonsave = nil
                    end,
                    desc = 'Save without trimming trailing whitespace',
                },
            },
        }
    end,

    hipatterns = function (hipatterns)
        return {
            opts = {
                highlighters = {
                    fixme     = { pattern = '%f[%S]()FIXME()%f[%W]', group = 'MiniHipatternsFixme' },
                    hack      = { pattern = '%f[%S]()HACK()%f[%W]',  group = 'MiniHipatternsHack'  },
                    todo      = { pattern = '%f[%S]()TODO()%f[%W]',  group = 'MiniHipatternsTodo'  },
                    note      = { pattern = '%f[%S]()NOTE()%f[%W]',  group = 'MiniHipatternsNote'  },
                    hex_color = hipatterns.gen_highlighter.hex_color(),
                },
            },
        }
    end,

    icons = function (icons)
        return {
            init = icons.mock_nvim_web_devicons,
        }
    end,

    tabline = function (_, M)
        return {
            hls = {
                { 'MiniTablineCurrent',         from = 'MiniTablineCurrent',        bg = M.style.default_bg, fg = M.style.colors.light2, underline = false, bold = false, italic = false },
                { 'MiniTablineModifiedCurrent', from = 'MiniTablineCurrent',        bg = M.style.default_bg, bold = true, italic = true },

                { 'MiniTablineVisible',         from = 'MiniTablineVisible',        bg = M.style.default_bg, fg = M.style.colors.gray2 },
                { 'MiniTablineModifiedVisible', from = 'MiniTablineVisible',        bg = M.style.default_bg, bold = true, italic = true },

                { 'MiniTablineHidden',          from = 'MiniTablineHidden',         bg = M.style.default_bg, fg = M.style.colors.gray2 },
                { 'MiniTablineModifiedHidden',  from = 'MiniTablineHidden',         bg = M.style.default_bg, bold = true, italic = true },

                { 'MiniTablineTabpagesection',  from = 'MiniTablineTabpagesection', bg = M.style.default_bg, },
            },
            opts = {
                show_icons = false,
            },
            -- autocmds = {
            --     {
            --         'BufEnter',
            --         vim.schedule_wrap(function()
            --             local n_listed_bufs = 0
            --             for _, buf_id in ipairs(vim.api.nvim_list_bufs()) do
            --                 if vim.fn.buflisted(buf_id) == 1 then
            --                     n_listed_bufs = n_listed_bufs + 1
            --                 end
            --             end
            --
            --             vim.o.showtabline = n_listed_bufs > 1 and 2 or 0
            --         end),
            --         desc = 'Only showtabline if more than one buffer is opened',
            --     },
            -- },
        }
    end,

    clue = function (miniclue, M)
        return {
            keymaps = {
                {
                    'n',
                    '<leader>tc',
                    function ()
                        vim.g.miniclue_disable = not vim.g.miniclue_disable

                        if not vim.g.miniclue_disable then
                            miniclue.ensure_all_triggers()
                        end
                    end,
                    desc = 'Toggle clues',
                },
            },

            opts = {
                triggers = {
                    -- Leader triggers
                    { mode = 'n', keys = '<leader>' },
                    { mode = 'x', keys = '<leader>' },

                    -- Built-in completions
                    { mode = 'i', keys = '<C-x>' },

                    -- `g` keys
                    { mode = 'n', keys = 'g' },
                    { mode = 'x', keys = 'g' },

                    -- Marks
                    { mode = 'n', keys = "'" },
                    { mode = 'n', keys = '`' },
                    { mode = 'x', keys = "'" },
                    { mode = 'x', keys = '`' },

                    -- Registers
                    { mode = 'n', keys = '"' },
                    { mode = 'x', keys = '"' },
                    { mode = 'i', keys = '<C-r>' },
                    { mode = 'c', keys = '<C-r>' },

                    -- Window commands
                    { mode = 'n', keys = '<C-w>' },

                    -- `z` keys
                    { mode = 'n', keys = 'z' },
                    { mode = 'x', keys = 'z' },
                },
                window = {
                    delay  = 700,
                    config = {
                        border = M.style.border,
                        width  = 40,
                    },
                },
                clues = {
                    miniclue.gen_clues.builtin_completion(),
                    miniclue.gen_clues.g(),
                    miniclue.gen_clues.marks(),
                    miniclue.gen_clues.registers(),
                    miniclue.gen_clues.windows(),
                    miniclue.gen_clues.z(),
                },
            },
        }
    end,

    pick = function (minipick, M)
        return {
            enabled = require('component.picker').implementor == 'mini',
            init    = function ()
                vim.ui.select = minipick.ui_select

                local paste_orig = vim.paste
                ---@diagnostic disable-next-line: duplicate-set-field
                vim.paste = function (...)
                    if not MiniPick.is_picker_active() then
                        return paste_orig(...)
                    end

                    for _, reg in ipairs({ '+', '.', '*' }) do
                        local content = vim.fn.getreg(reg) or ''
                        if content ~= '' then
                            MiniPick.set_picker_query({ content })
                            return
                        end
                    end

                    M.util.notify.warn('No content to paste')
                end
            end,

            opts = {
                mappings = {
                    choose_in_split  = '<C-v>',
                    choose_in_vsplit = '<C-s>',
                },

                window = {
                    prompt_prefix = ' > ',
                    config        = function ()
                        local width  = math.floor(0.618 * vim.o.columns)
                        local height = math.floor(0.618 * vim.o.lines)

                        return {
                            col      = math.floor(0.5 * (vim.o.columns - width)),
                            row      = math.floor(0.5 * (vim.o.lines - height)),
                            border   = M.style.border,
                            relative = 'editor',
                            height   = height,
                            width    = width,
                            anchor   = 'NW',
                        }
                    end,
                },
            }
        }
    end,

    statusline = function (statusline, M)
        local K = {
            git   = require('component.git'),
            icons = require('mini.icons'),

            get_num_bufs = function ()
                local counter = 0

                for _, buf in ipairs(vim.api.nvim_list_bufs()) do
                    if vim.bo[buf].buflisted and vim.api.nvim_buf_is_loaded(buf) and vim.api.nvim_buf_is_valid(buf) then
                        counter = counter + 1
                    end
                end

                return counter
            end,

            symbols = {
                unix = '', -- e712
                dos  = '', -- e70f
                mac  = '', -- e711
            },

            diagnostic = {
                [vim.diagnostic.severity.ERROR] = { icon = ' ', hl = 'DiagnosticSignError' },
                [vim.diagnostic.severity.WARN]  = { icon = ' ', hl = 'DiagnosticSignWarn'  },
                [vim.diagnostic.severity.INFO]  = { icon = ' ', hl = 'DiagnosticSignInfo'  },
                [vim.diagnostic.severity.HINT]  = { icon = ' ', hl = 'DiagnosticSignHint'  },
            },
        }

        local H = {
            get_bufs = function ()
                local num_bufs = K.get_num_bufs()
                local hl_bufs  = num_bufs > 8 and 'DiagnosticSignError' or num_bufs > 3 and 'MiniStatuslineUpdates' or 'MiniStatuslineProfile'

                return string.format('%%#%s# [%s]', hl_bufs, num_bufs)
            end,

            get_diagnostics = function ()
                if vim.bo.filetype == 'lazy' then
                    return ''
                end

                local counts = {}
                for _, item in ipairs(vim.diagnostic.get(0)) do
                    if K.diagnostic[item.severity] then
                        counts[item.severity] = (counts[item.severity] or 0) + 1
                    end
                end

                local diag = {}
                for _, level in ipairs({ vim.diagnostic.severity.ERROR, vim.diagnostic.severity.WARN, vim.diagnostic.severity.INFO, vim.diagnostic.severity.HINT }) do
                    local data = K.diagnostic[level]

                    if (counts[level] or 0) > 0 then
                        table.insert(diag, '%#' .. data.hl .. '# '  .. data.icon .. counts[level])
                    end
                end

                return table.concat(diag, '')
            end,

            get_lsp_names = function ()
                local names = M.lsp.get_client_names(0)

                if #names == 0 then
                    return ''
                end

                return '[' .. table.concat(names, ' / ') .. ']'
            end,

            create_item = function (str, hl, side)
                if (str or '') == '' or str == ' ' then
                    return ''
                end

                local prefix = ''
                if (hl or '') ~= '' then
                    prefix = '%#' .. hl .. '#'
                end

                if side == 'r' then
                    return prefix .. str .. ' '
                elseif side == 'l' then
                    return prefix .. ' ' .. str
                elseif side == 'b' then
                    return prefix .. ' ' .. str .. ' '
                end

                return prefix .. str
            end,
        }

        return {
            opts = {
                set_vim_settings = false,
                content          = {
                    active = function ()
                        local filetype = vim.bo.filetype or ''

                        if filetype == 'ministarter' then
                            return ''
                        end

                        local search        = statusline.section_searchcount({ trunc_width = 75 })
                        local mode, mode_hl = statusline.section_mode({ trunc_width = 120 })
                        local encoding      = (vim.bo.fileencoding or '') == '' and vim.go.encoding or vim.bo.fileencoding
                        local filename      = vim.bo.buftype == 'terminal' and '%t' or "%{expand('%:~:.')}%m%r"
                        local file_icon     = filetype == '' and '' or (K.icons.get('filetype', filetype) or '')
                        local newline       = K.symbols[vim.bo.fileformat] or '?'
                        local git           = K.git.branch_name(' %s', false)
                        local reg           = vim.fn.reg_recording()
                        local cur_line      = vim.fn.line('.')
                        local last_line     = vim.fn.line('$')
                        local cur_col       = vim.fn.col('.')
                        local progress      = (cur_line == 1 and 'top') or (cur_line == last_line and 'bot') or string.format('%2d%%%%', math.floor(cur_line / last_line * 100))
                        local last_line_len = #tostring(last_line)

                        if search ~= '' then
                            search = string.format('[%s]', search)
                        end

                        return table.concat({
                            H.create_item(mode, mode_hl, 'b'),
                            H.create_item(git, 'MiniStatuslineDevinfo', 'b'),
                            '%<', -- Mark general truncate point
                            H.create_item(H.get_bufs()),
                            ' %#MiniStatuslineFilename##%n ' .. filename,
                            H.create_item(file_icon, 'MiniStatuslineProfile', 'l'),
                            H.create_item(vim.b.fqclassname and ' ' .. vim.b.fqclassname or '', 'MiniStatuslineFilename', 'l'),
                            H.create_item(H.get_diagnostics()),
                            H.create_item(search, 'MiniStatuslineSearch', 'l'),
                            '%=', -- End left alignment
                            H.create_item(reg == '' and '' or string.format(' 📽️ @%s ', reg), 'MiniStatuslineRecording'),
                            H.create_item(vim.g.version, 'MiniStatuslineFilename', 'b'),
                            H.create_item(M.util.get_updates(), 'MiniStatuslineUpdates', 'r'),
                            H.create_item(vim.o.mouse == '' and '' or '🐭', 'MiniStatuslineProfile', 'r'),
                            H.create_item(H.get_lsp_names(), 'MiniStatusLspnames', 'r'),
                            H.create_item(string.format('%s %s ', encoding, newline), 'MiniStatuslineDevinfo', 'b'),
                            H.create_item(progress, mode_hl, 'b'),
                            H.create_item(string.format('%' .. last_line_len .. 's:%-3d', cur_line, cur_col)),
                        }, '')
                    end,
                },
            },

            hls = {
                { 'MiniStatuslineRecording',    bg = M.style.colors.highlight2, fg = M.style.colors.dark1, bold = true, italic = true },
                { 'MiniStatuslineDevInfo',      bg = M.style.colors.dark1,      fg = M.style.colors.highlight3 },
                { 'MiniStatuslineFilename',     bg = M.style.default_bg,        fg = M.style.colors.gray1      },
                { 'MiniStatusLspnames',         bg = M.style.default_bg,        fg = M.style.colors.gray1      },
                { 'MiniStatuslineProfile',      bg = M.style.default_bg,        fg = M.style.colors.highlight1 },
                { 'MiniStatuslineUpdates',      bg = M.style.default_bg,        fg = M.style.colors.highlight2 },
                { 'MiniStatuslineSearch',       bg = M.style.default_bg,        fg = M.style.colors.highlight1 },

                { 'MiniStatuslineModeInsert',  bold = function () return nil end, from = 'MiniStatuslineModeInsert', bg = M.style.colors.highlight1  },
                { 'MiniStatuslineModeOther',   bold = function () return nil end, from = 'MiniStatuslineModeOther'   },
                { 'MiniStatuslineModeNormal',  bold = function () return nil end, from = 'MiniStatuslineModeNormal'  },
                { 'MiniStatuslineModeVisual',  bold = function () return nil end, from = 'MiniStatuslineModeVisual'  },
                { 'MiniStatuslineModeCommand', bold = function () return nil end, from = 'MiniStatuslineModeCommand' },
                { 'MiniStatuslineModeReplace', bold = function () return nil end, from = 'MiniStatuslineModeReplace' },
            },
        }
    end,

    starter = function (starter, M)
        local greeter = require('component.greeter')
        local items   = greeter.items(starter.open)
        local headers = greeter.headers(true)
        local width   = 0

        for _, value in ipairs(headers) do
            if #value > width then
                width = #value
            end
        end

        return {
            hls = {
                { 'MiniStarterItem', fg = M.style.colors.gray4 },
            },
            keymaps = {
                {
                    'n',
                    '<leader>ad',
                    greeter.opener(starter.open),
                    desc = 'Close all buffers and open dashboard',
                },
            },

            autocmds = {
                {
                    'User',
                    function (event)
                        vim.b.miniindentscope_disable = true
                        require('component.closer').closeAll({ bufToSkip = event.buf })

                        M.cfg.autocmd({
                            'ModeChanged',
                            function ()
                                if vim.tbl_contains({ 'v', 'V', 'i', 'R' }, vim.api.nvim_get_mode()['mode']) then
                                    vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<ESC>', true, false, true), 'x', true)
                                end
                            end,
                            desc   = 'Only have normal mode on the dashboard',
                            buffer = event.buf,
                        })

                        M.cfg.keymaps({
                            {
                                'n',
                                '<C-p>',
                                M.files.find_files,
                                desc   = 'Find files',
                                buffer = event.buf,
                            },
                            {
                                'n',
                                'j',
                                function ()
                                    starter.update_current_item('next')
                                end,
                                desc   = 'Next item',
                                buffer = event.buf,
                            },
                            {
                                'n',
                                'k',
                                function ()
                                    starter.update_current_item('prev')
                                end,
                                desc   = 'Previous item',
                                buffer = event.buf,
                            },
                        })
                    end,
                    pattern = 'MiniStarterOpened',
                },
            },

            opts = {
                query_updaters  = 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ/',
                content_hooks   = { starter.gen_hook.aligning('center', 'top') },
                autoopen        = false,
                evaluate_single = true,
                silent          = true,
                items           = (function (center)
                    local elements = {}

                    for _, item in ipairs(center) do
                        if item[1] == nil then
                            table.insert(elements, {
                                action  = 'nop',
                                name    = '',
                                section = '',
                            })
                        else
                            local str = item[3] .. ' ' .. string.rep('.', width - 3 - #item[1]) .. ' ' .. item[1]

                            table.insert(elements, {
                                name    = str .. string.rep(' ', width - vim.fn.strdisplaywidth(str)),
                                action  = item[2],
                                section = '',
                            })
                        end
                    end

                    return elements
                end)(items),

                header = (function (center)
                    local total_height         = vim.api.nvim_win_get_height(0)
                    local target_header_height = math.ceil((total_height - #center * 2) / 2)
                    local header_height        = #headers

                    if header_height >= target_header_height then
                        target_header_height = header_height + 5
                    end

                    local lines_to_add = target_header_height - header_height
                    if lines_to_add > 2 then
                        lines_to_add = lines_to_add - 2

                        table.insert(headers, '')
                        table.insert(headers, '')
                    end

                    for _ = 1, lines_to_add do
                        table.insert(headers, 1, '')
                    end

                    return table.concat(headers, '\n')
                end)(items),

                footer = function ()
                    local footer = {}

                    for _, line in ipairs(greeter.footer()) do
                        local len = vim.fn.strdisplaywidth(line)
                        if len > width then
                            line = line:sub(0, width - 4) .. ' ...'
                            len = vim.fn.strdisplaywidth(line)
                        end

                        local pad = string.rep(' ', math.floor((width - len) / 2))

                        table.insert(footer, string.format('%s%s%s', pad, line, pad))
                    end

                    return table.concat(footer, '\n')
                end,
            },

            init = function ()
                greeter.set_handlers_to_lauch_after_last_buffer_close(
                    'ministarter',
                    starter.open
                )

                vim.schedule(function ()
                    if vim.bo.filetype == '' then
                        starter.open()
                    end
                end)
            end,
        }
    end,

    notify = function (notify, M)
        return {
            opts = {
                content = {
                    format = function (notification)
                        if notification.data.source == 'lsp_progress' then
                            local s_pos, e_pos = string.find(notification.msg, notification.data.client_name .. ': ', 1, true)

                            if s_pos ~= nil and e_pos ~= nil then
                                return string.sub(notification.msg, e_pos + 1)
                            end
                        end

                        return notification.msg
                    end,
                },
                window = {
                    winblend = 0,
                    config   = function ()
                        local pad = vim.o.cmdheight + (vim.o.laststatus > 0 and 1 or 0)

                        return {
                            row    = vim.o.lines - pad - 1,
                            col    = vim.o.columns,
                            border = 'none',
                            anchor = 'SE',
                        }
                    end,
                },
            },
            autocmds = {
                {
                    'ColorScheme',
                    function ()
                        M.cfg.set_hls({
                            { 'MiniNotifyBorder', { bg = M.style.default_bg } },
                            { 'MiniNotifyNormal', { bg = M.style.default_bg } },
                            { 'MiniNotifyTitle',  { bg = M.style.default_bg } },
                        })
                    end,
                },
            },
            init = function ()
                vim.notify = notify.make_notify({ INFO = { duration = 10000 } })
            end,
        }
    end,

    cursorword = {
        hls = {
            { 'MiniCursorwordCurrent', link = 'Visual' },
            { 'MiniCursorword',        link = 'Visual' },
        },
    },

    bufremove = function (bufremove)
        return {
            enabled = false,
            keymaps = {
                {
                    'n',
                    'Q',
                    function ()
                        if vim.bo.filetype == 'dashboard' or vim.bo.filetype == 'ministarter' then
                            vim.api.nvim_command('quitall')
                        elseif not vim.bo.modified then
                            bufremove.delete(0)
                        else
                            local choice = vim.fn.confirm(string.format('Save changes to %q?', vim.fn.bufname()), '&Yes\n&No\n&Cancel')
                            if choice == 1 then
                                vim.api.nvim_command('write ++p')
                                bufremove.delete(0)
                            elseif choice == 2 then
                                bufremove.delete(0, true)
                            end
                        end
                    end,
                    desc = 'Close buffer',
                },
            },
        }
    end,

    files = function (files, M)
        return {
            opts = {
                enabled = false,
                content = {
                    filter = function (fs_entry)
                        return fs_entry.fs_type ~= 'directory' or not vim.startswith(fs_entry.name, '.')
                    end,
                },
                mappings = {
                    go_in  = '',
                    go_out = '',
                },
            },

            autocmds = {
                {
                    'User',
                    function (args)
                        local H = {
                            override_confirm = function (answer, callback)
                                local conf = vim.fn.confirm
                                ---@diagnostic disable-next-line: duplicate-set-field
                                vim.fn.confirm = function () return answer end
                                callback()
                                vim.fn.confirm = conf
                            end,
                        }

                        local move_up_or_close = function ()
                            for _ = 1, vim.v.count1 do
                                local cur_win_id = vim.api.nvim_get_current_win()
                                local state      = files.get_explorer_state()

                                if state == nil then
                                    return
                                end

                                for _, win_data in ipairs(state.windows) do
                                    if win_data.win_id == cur_win_id then
                                        if win_data.path == state.anchor then
                                            H.override_confirm(1, files.close)
                                        else
                                            files.go_out()
                                        end

                                        return
                                    end
                                end
                            end
                        end

                        local open_in_split = function (direction)
                            if files.get_fs_entry().fs_type ~= 'file' then
                                return
                            end

                            local cur_target = files.get_explorer_state().target_window
                            local new_target = vim.api.nvim_win_call(
                                cur_target,
                                function()
                                    vim.api.nvim_command(direction .. ' split')
                                    return vim.api.nvim_get_current_win()
                                end
                            )

                            files.set_target_window(new_target)
                            files.go_in({ close_on_file = true })

                            vim.api.nvim_set_current_win(new_target)
                        end

                        M.cfg.keymaps({
                            {
                                'n',
                                '<C-v>',
                                function () open_in_split('belowright horizontal') end,
                                desc   = 'Open in horizontal split',
                                buffer = args.data.buf_id,
                            },
                            {
                                'n',
                                '<C-s>',
                                function () open_in_split('belowright vertical') end,
                                desc   = 'Open in vertical split',
                                buffer = args.data.buf_id,
                            },

                            {
                                'n',
                                '<Left>',
                                move_up_or_close,
                                desc   = 'Move to parent directory',
                                buffer = args.data.buf_id,
                            },
                            {
                                'n',
                                '<ESC>',
                                move_up_or_close,
                                desc   = 'Move to parent directory',
                                buffer = args.data.buf_id,
                            },
                            {
                                'n',
                                '<CR>',
                                function ()
                                    files.go_in({ close_on_file = true })
                                end,
                                buffer = args.data.buf_id,
                                desc   = 'Open entry',
                            },
                            {
                                'n',
                                '<Right>',
                                function ()
                                    if files.get_fs_entry().fs_type == 'directory' then
                                        files.go_in({ close_on_file = true })
                                    end
                                end,
                                buffer = args.data.buf_id,
                                desc   = 'Open entry',
                            },
                            {
                                'n',
                                '=',
                                function ()
                                    H.override_confirm(1, files.synchronize)
                                end,
                                desc   = 'Synchronize without confirmation',
                                buffer = args.data.buf_id,
                            },
                        })
                    end,
                    pattern = 'MiniFilesBufferCreate',
                }
            },
        }
    end,

    visits = function (visits)
        return {
            enabled = false,
            opts    = {
                sort   = visits.gen_sort.default({ recency_weight = 1 }),
                filter = function (item)
                    if item.path:match '%.git' then
                        return false
                    end

                    if vim.loop.fs_stat(item.path) then
                        return true
                    end

                    return false
                end,
            },
        }
    end,

    pairs = {
        enabled = false,
    },

    diff = {
        enabled = false,
        opts    = {
            view = { style = 'sign' },
        },
    },

    git = function (git)
        return {
            enabled = false,
            keymaps = {
                {
                    'n',
                    '<leader>gs',
                    function ()
                        git.show_at_cursor({ split = 'vertical' })
                    end,
                    desc = 'Show git info at cursor',
                },
            },
        }
    end,

    indentscope = function (indentscope, M)
        return {
            enabled  = false,
            autocmds = {
               {
                    'filetype',
                    function ()
                        vim.b.miniindentscope_disable = true
                    end,
                    pattern = {
                        'help',
                        'alpha',
                        'dashboard',
                        'neo-tree',
                        'trouble',
                        'trouble',
                        'lazy',
                        'mason',
                        'notify',
                        'toggleterm',
                        'lazyterm',
                    },
                },
            },

            keymaps = {
                {
                    'n',
                    '<leader>ti',
                    function ()
                        vim.b.miniindentscope_disable = not vim.b.miniindentscope_disable

                        if vim.b.miniindentscope_disable then
                            indentscope.undraw()
                        else
                            indentscope.draw()
                        end
                    end,
                    desc = 'Toggle indent lines',
                },
            },

            hls = {
                { 'MiniIndentscopeSymbol', fg = M.style.colors.dark2 },
            },

            opts = {
                options = { try_as_border = true },
                symbol  = '│',
                draw    = {
                    animation = function ()
                        return 10
                    end,
                },
            },
        }
    end,
})
