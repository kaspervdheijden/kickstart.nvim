return (function (style, get_ignored_items)
    return {
        'saghen/blink.cmp',
        event   = 'InsertEnter',
        build   = false,
        version = '*',
        opts    = {
            completion = {
                menu = {
                    border = style.border,
                    draw   = {
                        treesitter = { 'lsp' },
                        columns    = {
                            { 'label',     'label_description', gap = 1 },
                            { 'kind_icon', 'kind',              gap = 1 },
                        },
                    },
                    auto_show = function (ctx)
                        return ctx.mode ~= 'cmdline' and not vim.tbl_contains({ '/', '?' }, vim.fn.getcmdtype())
                    end,
                },
                documentation = {
                    window             = { border = style.border },
                    auto_show          = true,
                    auto_show_delay_ms = 200,
                },
            },
            signature = {
                window  = { border = style.border },
                enabled = true,
            },
            sources = {
                providers = {
                    snippets = {
                        opts = {
                            search_paths = {
                                vim.env.NVIM_CUSTOM_SNIPPETS_DIR or (vim.env.HOME .. '/.config/dotfiles-local-config/snippets'),
                                vim.fn.stdpath('config') .. '/snippets',
                            },
                        },
                    },
                    lsp = {
                        transform_items = function (_, items)
                            local ignoredItems = get_ignored_items()

                            return vim.tbl_filter(function (item) return ignoredItems[item.label] == nil end, items)
                        end,
                    },
                },
            },
            keymap = {
                ['<Tab>'] = { 'select_and_accept', 'fallback', },
                ['<C-y>'] = { 'select_and_accept' },
                preset    = 'enter',
            },
            cmdline = {
                keymap = {
                    ['<Tab>']   = { 'show', 'select_next' },
                    ['<C-y>']   = { 'select_and_accept' },
                    ['<C-e>']   = { 'hide', 'fallback' },
                    ['<S-Tab>'] = { 'select_prev' },
                    preset      = 'enter',
                },
            },
        },
    }
end)(
    require('config.style'),
    (function (ignored)
        return function ()
            if ignored == nil then
                ignored = {}

                if vim.env.NVIM_IGNORED_LSP_LABELS_FILE ~= nil then
                    local file = io.open(vim.fn.expand(vim.env.NVIM_IGNORED_LSP_LABELS_FILE), 'r')

                    if file then
                        for label in file:lines() do
                            if label ~= '' then
                                ignored[label] = 1
                            end
                        end

                        file:close()
                    end
                end
            end

            return ignored
        end
    end)(nil)
)
