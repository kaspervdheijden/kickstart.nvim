return {
    'lewis6991/gitsigns.nvim',
    event = { 'BufNewFile', 'BufReadPost' },
    opts  = {
        current_line_blame_formatter = '<author>, <author_time:%R> - <abbrev_sha> "<summary>"',
        current_line_blame           = true,
        current_line_blame_opts      = {
            virt_text_pos = 'right_align', -- 'eol' | 'overlay' | 'right_align'
            delay         = 200,
        },
        on_attach = function (buf)
            local gitsigns = require('gitsigns')

            require('component.configurator').keymaps({
                {
                    'n',
                    ']h',
                    function()
                        if vim.wo.diff then
                            vim.cmd.nvim_command('normal! ]c')
                        else
                            gitsigns.nav_hunk('next')
                        end
                    end,
                    desc   = 'Jump to next hunk',
                    buffer = buf,
                },
                {
                    'n',
                    '[h',
                    function()
                        if vim.wo.diff then
                            vim.api.nvim_command('normal! [c')
                        else
                            gitsigns.nav_hunk('prev')
                        end
                    end,
                    desc   = 'Jump to previous hunk',
                    buffer = buf,
                },
                {
                    'n',
                    '<leader>gl',
                    function ()
                        for _, bufnr in ipairs(vim.api.nvim_list_bufs()) do
                            if vim.bo[bufnr].filetype == 'gitsigns-blame' then
                                require('component.closer').close({ buf = bufnr })
                                return
                            end
                        end

                        gitsigns.blame()
                    end,
                    desc = 'Toggle blame',
                },
            })
        end,
    },
}
