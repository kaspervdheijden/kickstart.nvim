return {
    'williamboman/mason.nvim',
    event        = 'BufReadPost',
    cmd          = 'Mason',
    dependencies = {
        'williamboman/mason-lspconfig.nvim',
        'neovim/nvim-lspconfig',
    },
    keys = {
        { '<leader>am', '<cmd>Mason<CR>', desc = 'Mason' },
    },
    config = function ()
        require('mason').setup({
            health = {
                ignore = {
                    'luarocks',
                    'RubyGem',
                    'cargo',
                    'javac',
                    'julia',
                    'Go',
                },
            },
            ui = {
                border = require('config.style').border,
                height = 0.75,
                width  = 0.8,
            },
        })

        local cfg       = require('component.configurator')
        local lsp       = require('component.lsp')
        local on_attach = function (client, buf)
            cfg.keymaps({
                { 'n',               '<leader>cm', vim.lsp.buf.rename,         buffer = buf, desc = 'LSP: Rename',                  },
                { 'n',               '<leader>ca', vim.lsp.buf.code_action,    buffer = buf, desc = 'LSP: Code action'              },
                { { 'i', 'n', 'v' }, '<M-CR>',     vim.lsp.buf.code_action,    buffer = buf, desc = 'LSP: Code action',             },
                { 'n',               'gd',         lsp.definitions,            buffer = buf, desc = 'LSP: Goto definition',         },
                { 'n',               'gr',         lsp.references,             buffer = buf, desc = 'LSP: Goto references',         },
                { 'n',               'gt',         lsp.implementations,        buffer = buf, desc = 'LSP: Goto implementation',     },
                { 'n',               'gc',         vim.lsp.buf.declaration,    buffer = buf, desc = 'LSP: Goto declaration',        },
                { 'n',               'gf',         lsp.type_definitions,       buffer = buf, desc = 'LSP: Goto type definition',    },
                { 'n',               '<leader>sd', lsp.document_symbols,       buffer = buf, desc = 'LSP: Document symbols',        },
                { 'n',               '<leader>sp', lsp.project_symbols,        buffer = buf, desc = 'LSP: Project symbols',         },
                { 'n',               '<leader>cd', lsp.diagnostics,            buffer = buf, desc = 'Diagnostics',                  },
                { 'n',               '<leader>sq', lsp.quickfix,               buffer = buf, desc = 'Quickfix',                     },
                { 'n',               '<leader>cr', lsp.restart,                buffer = buf, desc = 'LSP: Restart',                 },
                { { 'i', 'n', 'v' }, '<F5>',       lsp.restart,                buffer = buf, desc = 'LSP: Restart',                 },
                { { 'i', 'n', 'v' }, '<C-k>',      vim.lsp.buf.signature_help, buffer = buf, desc = 'LSP: Signature documentation', }, -- See `:help K` for why this keymap
                { { 'i', 'n', 'v' }, '<C-l>',      vim.lsp.buf.hover,          buffer = buf, desc = 'LSP: Hover documentation',     },
                { 'n',               'K',          vim.lsp.buf.hover,          buffer = buf, desc = 'LSP: Hover documentation',     },
                { { 'n', 'v' },      '[d',         lsp.diagnostics_prev,       buffer = buf, desc = 'Previous diagnostic'           },
                { { 'n', 'v' },      ']d',         lsp.diagnostics_next,       buffer = buf, desc = 'Next diagnostic'               },
            })

            if client.server_capabilities.documentHighlightProvider then
                cfg.autocmds(
                    {
                        {
                            { 'CursorHold', 'CursorHoldI' },
                            vim.lsp.buf.document_highlight,
                            buffer = buf,
                        },
                        {
                            { 'CursorMoved', 'CursorMovedI' },
                            vim.lsp.buf.clear_references,
                            buffer = buf,
                        },
                    },
                    'lsp-doc-highlight'
                )
            end
        end

        local lsp_config   = require('lspconfig')
        local capabilities = lsp.capabilities()
        local servers      = lsp.servers()

        require('mason-lspconfig').setup({
            ensure_installed       = vim.tbl_keys(servers),
            automatic_installation = true,
            handlers               = {
                function (server_name)
                    local server = servers[server_name] or {}

                    server.telemetry = { enabled = false }

                    lsp_config[server_name].setup({
                        init_options = server.init_options,
                        filetypes    = server.filetypes,
                        capabilities = capabilities,
                        on_attach    = on_attach,
                        settings     = server,
                    })
                end,
            },
        })
    end,
}
