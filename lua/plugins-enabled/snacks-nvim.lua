return (function (style)
    return {
        'folke/snacks.nvim',
        priority = 1000,
        opts = {
            explorer = { enabled = true },
            input    = {
                border    = style.border,
                title_pos = 'center',
                relative  = 'editor',
                position  = 'float',
                backdrop  = false,
                noautocmd = true,
                width     = 60,
                height    = 1,
                row       = 2,
                wo        = {
                    winhighlight = 'NormalFloat:SnacksInputNormal,FloatBorder:SnacksInputBorder,FloatTitle:SnacksInputTitle',
                    cursorline   = false,
                },
                bo = {
                    filetype = 'snacks_input',
                    buftype  = 'prompt',
                },
                b = {
                    completion = false,
                },
                keys = {
                    i_esc    = { '<ESC>',  { 'cmp_close', 'stopinsert' }, mode = 'i', expr = true },
                    i_tab    = { '<TAB>',  { 'cmp_select_next', 'cmp' },  mode = 'i', expr = true },
                    i_cr     = { '<CR>',   { 'cmp_accept', 'confirm' },   mode = 'i', expr = true },
                    n_esc    = { '<ESC>',  { 'cmp_close', 'cancel' },     mode = 'n', expr = true },
                    i_ctrl_w = { '<C-w>',  '<C-S-w>',                     mode = 'i', expr = true },
                    i_down   = { '<DOWN>', { 'hist_down' },               mode = { 'i', 'n' } },
                    i_up     = { '<UP>',   { 'hist_up' },                 mode = { 'i', 'n' } },
                    q        = 'cancel',
                },
            },
            picker = {
                layouts = {
                    default = {
                        layout = {
                            box       = 'horizontal',
                            height    = 0.85,
                            width     = 0.8,
                            min_width = 120,
                            {
                                title  = '{title} {live} {flags}',
                                border = style.border,
                                box    = 'vertical',
                                { win = 'input', border = 'bottom', height = 1 },
                                { win = 'list',  border = 'none' },
                            },
                            {
                                border = style.border,
                                win    = 'preview',
                                width  = 0.5,
                                title  = '',
                            },
                        },
                    },
                },
                formatters = {
                    file = {
                        filename_first = true,
                        truncate       = 180,
                    },
                },
                cwd     = vim.fn.getcwd(),
                matcher = {
                    smartcase     = false,
                    frecency      = true,
                    history_bonus = true,
                },
                win = {
                    input = {
                        keys = {
                            ['<C-s>'] = { 'edit_vsplit', mode = { 'i', 'n' } },
                            ['<C-v>'] = { 'edit_split',  mode = { 'i', 'n' } },
                            ['<ESC>'] = { 'close',       mode = { 'n', 'i' } },
                        },
                    },
                    preview = {
                        wo = { number = false },
                    },
                },
            },
        },
    }
end)(require('config.style'))
