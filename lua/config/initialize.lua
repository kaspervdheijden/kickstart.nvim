return (function (cfg, style)
    local function set_hls ()
        cfg.set_hls({
            { 'YankColor',        bg = style.colors.highlight1, fg = style.colors.gray4 },
            { 'ColorColumn',      bg = style.colors.dark3 },
            { 'StatusLine',       bg = style.default_bg   },
            { 'Normal',           bg = style.default_bg   },
            { 'MiniNotifyBorder', bg = style.default_bg   },
            { 'MiniNotifyNormal', bg = style.default_bg   },
            { 'MiniNotifyTitle',  bg = style.default_bg   },
        })
    end

    return {
        init = function ()
            vim.api.nvim_command('aunmenu PopUp')

            cfg.register_key_groups({
                { '<leader>c',  'Code', mode = { 'n', 'v' } },
                { '<C-\\>',     'Quick commands'            },
                { '<leader>a',  'Application'               },
                { '<leader>s',  'Search'                    },
                { '<leader>t',  'Toggle'                    },
                { '<leader>c',  'Code'                      },
                { '<leader>f',  'File'                      },
                { '<leader>g',  'Git'                       },
                { '<leader>gr', 'Unstage'                   },
                { '<leader>ga', 'Stage'                     },
            })

            set_hls()
        end,

        on_colorscheme = set_hls,
    }
end)(require('component.configurator'), require('config.style'))
