return (function (keymap, create_entry, items, definitions)
    local picker = require('component.picker')

    for _, def in ipairs(definitions) do
        local path = ''

        for _, candidate in ipairs(def.paths) do
            if path == '' and vim.fn.isdirectory('./' .. candidate) == 1 then
                path = candidate
            end
        end

        if path ~= '' then
            table.insert(keymap, {
                'n',
                def.keys,
                function ()
                    picker.files({ cwd = path, hidden = true })
                end,
                desc = 'Search in "' .. path .. '"',
            })
        end
    end

    for _, item in ipairs(items) do
        table.insert(keymap, create_entry(item))
    end

    return keymap
end)((function ()
    local closer = require('component.closer')
    local picker = require('component.picker')
    local files  = require('component.files')
    local util   = require('component.util')
    local git    = require('component.git')

    return {
        --
        -- Git
        --
        {
            'n',
            '<leader>gb',
            git.switch_branch,
            desc = 'Switch branch',
        },
        {
            'n',
            '<leader>go',
            git.remote_url,
            desc = 'Browse to remote repo url',
        },
        {
            'n',
            '<leader>gap',
            function ()
                git.add({ interactive = true })
            end,
            desc = 'Stage hunks in current file',
        },
        {
            'n',
            '<leader>gaf',
            git.add,
            desc = 'Stage entire current file',
        },
        {
            'n',
            '<leader>grp',
            function ()
                git.unstage({ interactive = true})
            end,
            desc = 'Unstage hunks in current file',
        },
        {
            'n',
            '<leader>grf',
            git.unstage,
            desc = 'Unstage entire current file',
        },
        {
            'n',
            '<leader>gc',
            git.checkout,
            desc = 'Restore current buffer',
        },

        --
        -- Toggle
        --
        {
            'n',
            '<leader>tm',
            function ()
                vim.o.mouse = vim.o.mouse == 'a' and '' or 'a'
            end,
            desc = 'Toggle mouse',
        },
        {
            'n',
            '<leader>td',
            function ()
                vim.diagnostic.enable(not vim.diagnostic.is_enabled())
            end,
            desc = 'Toggle diagnostics',
        },
        {
            'n',
            '<leader>tv',
            (function (replacers)
                return function ()
                    local curWord = vim.fn.expand('<cword>')

                    for word, replace in pairs(replacers) do
                        if curWord == word then
                            local back = ''

                            if #replace > 1 then
                                back = 'b'
                            end

                            return 'ciw' .. replace .. '<ESC>' .. back
                        end
                    end
                end
            end)({
                ['yes']   = 'no',
                ['Yes']   = 'No',
                ['YES']   = 'NO',
                ['no']    = 'yes',
                ['No']    = 'Yes',
                ['NO']    = 'YES',

                ['on']    = 'off',
                ['On']    = 'Off',
                ['ON']    = 'OFF',
                ['off']   = 'on',
                ['Off']   = 'On',
                ['OFF']   = 'ON',

                ['true']  = 'false',
                ['True']  = 'False',
                ['TRUE']  = 'FALSE',
                ['false'] = 'true',
                ['False'] = 'True',
                ['FALSE'] = 'TRUE',

                ['0']     = '1',
                ['1']     = '0',
            }),
            desc   = 'Toggle value under cursor',
            silent = true,
            expr   = true,
        },

        --
        -- Search
        --
        {
            { 'n', 'v' },
            '<leader>sg',
            files.grep,
            desc = 'Search from (git) root',
        },
        {
            { 'n', 'i', 'v', 'x' },
            '<A-p>',
            picker.buffers,
            desc = 'Search open buffers',
        },
        {
            { 'n', 'i', 'v', 'x' },
            '<C-p>',
            picker.files,
            desc = 'Search',
        },
        {
            { 'n', 'i', 'v', 'x' },
            '<C-n>',
            picker.resume,
            desc = 'Search resume',
        },
        {
            'n',
            '<leader>sw',
            picker.search_word,
            desc = 'Search for word under cursor',
        },

        --
        -- Code
        --
        { 'n', '<leader>cq', '<cmd>copen<CR>', desc = 'Open quickfix list' },

        --
        -- File
        --
        {
            'n',
            '<leader>fc',
            function ()
                util.copy_and_notify(vim.fn.expand('%') or '', 'filename')
            end,
            desc = 'Copy filename',
        },
        {
            'n',
            '<leader>fd',
            function ()
                local filename = vim.fn.expand('%')

                if (filename or '') ~= '' then
                    local buf = vim.api.nvim_get_current_buf()

                    vim.system(
                        { 'rm', filename },
                        { text = true },
                        vim.schedule_wrap(function (result)
                            closer.close({ buf = buf })

                            if result.code == 0 then
                                util.notify.info('Deleted: "' .. filename  .. '"')
                            else
                                util.notify.err('Could not delete "' .. filename .. '"')
                            end
                        end)
                    )
                end
            end,
            desc = 'Delete current file',
        },

        --
        -- Generic
        --
        { 'x', '<leader>p', '"_dp',                 desc = 'Paste selection', silent = true },
        { 'v', '<leader>c', '+ygv',                 desc = 'Copy selection'                 },

        --
        -- Application
        --
        { 'n', '<leader>an', '<cmd>messages<CR>',    desc = 'Notifications' },
        { 'n', '<leader>ah', '<cmd>checkhealth<CR>', desc = 'Check health', },
        { 'n', '<leader>as', '<cmd>Lazy sync<CR>',   desc = 'Lazy sync'     },
        { 'n', '<leader>al', '<cmd>Lazy<CR>',        desc = 'Lazy'          },

        {
            'n',
            'T',
            function ()
                return 'i' .. string.rep(' ', vim.o.shiftwidth) .. '<ESC>l'
            end,
            desc = 'Add shiftwidth number of spaces (' .. vim.o.shiftwidth .. ')',
            expr = true,
        },

        {
            { 'n', 'v', 'i', 't', },
            '<C-t>',
            util.toggle_float_term,
            desc = 'Floating terminal',
        },
        {
            { 'n', 'i', 'v' },
            '<C-g>',
            files.explorer,
            desc   = 'Toggle file explorer',
            silent = true,
        },

        {
            { 'n', 'v', 'i' },
            '<C-A-t>',
            function ()
                local fn = require('component.stack').pop('buffers')

                if fn ~= nil then
                    vim.api.nvim_command('e ' .. fn)
                else
                    util.notify.warn('No recently closed buffers')
                end
            end,
            desc = 'Open last closed buffer',
        },

        { 'n',                  '<C-Down>',     '<cmd>m .+1<CR>==',            desc = 'Move down', silent = true },
        { 'n',                  '<C-Up>',       '<cmd>m .-2<CR>==',            desc = 'Move up',   silent = true },
        { 'i',                  '<C-Down>',     '<ESC><cmd>m .+1<CR>==gi',     desc = 'Move down', silent = true },
        { 'i',                  '<C-Up>',       '<ESC><cmd>m .-2<CR>==gi',     desc = 'Move up',   silent = true },
        { 'v',                  '<C-Down>',     ":m '>+1<CR>gv=gv",            desc = 'Move down', silent = true },
        { 'v',                  '<C-Up>',       ":m '<-2<CR>gv=gv",            desc = 'Move up',   silent = true },
        { 'n',                  '<bs>',         'X',                           desc = 'Backspace'                },

        -- { 'n',                  'cfw',          '^ciw',                        desc = 'Change first word' },
        -- { 'n',                  'dfw',          '^dw',                         desc = 'Delete first word' },
        -- { 'n',                  'clw',          '$ciw',                        desc = 'Change last word'  },
        -- { 'n',                  'dlw',          '$diw',                        desc = 'Delete last word'  },

        { 'n',                  'j',            "v:count == 0 ? 'gj' : 'j'",   desc = 'Move one line down', expr = true, silent = true },
        { 'n',                  'k',            "v:count == 0 ? 'gk' : 'k'",   desc = 'Move one line up',   expr = true, silent = true },

        { 'v',                  '<',            '<gv',                         desc = 'Unindent selection' },
        { 'v',                  '>',            '>gv',                         desc = 'Indent selection'   },

        { 'n',                  'L',            '^v$h',                        desc = 'Select line' },
        { 'v',                  'L',            '<ESC>',                       desc = 'Select line' },

        -- { 'v',                  '/',            '<ESC>/\\%V',                  desc = 'Search in selection'      },
        { 'v',                  '<S-Down>',     'j',                           desc = 'Move selection down one line' },
        { 'v',                  '<S-Up>',       'k',                           desc = 'Move selection up one line'   },
        { 'i',                  '<S-Down>',     '<ESC>vj',                     desc = 'Select line down' },
        { 'n',                  '<S-Down>',     'vj',                          desc = 'Select line down' },
        { 'i',                  '<S-Up>',       '<ESC>vk',                     desc = 'Select line up'   },
        { 'n',                  '<S-Up>',       'vk',                          desc = 'Select line up'   },

        { { 'n', 'v', 'i' },    '<C-s>',        '<cmd>w ++p<CR>',              desc = 'Save file' },
        { { 'n', 'v', 'i' },    '<C-S-s>',      '<cmd>wa<CR>',                 desc = 'Save all'  },

        { 'i',                  '<S-Tab>',      '<C-d>',                       desc = 'Undent code', noremap = true },
        { 'v',                  '<S-Tab>',      '<gv',                         desc = 'Unindent code' },
        { 'v',                  '<Tab>',        '>gv',                         desc = 'Indent code' },
        { 'n',                  '<S-Tab>',      '<<',                          desc = 'Unindent code' },
        { 'n',                  '<Tab>',        '>>',                          desc = 'Indent code' },

        { 'n',                  '<C-S-Up>',     '<cmd>resize +2<CR>',          desc = 'Increase window height', silent = true },
        { 'n',                  '<C-S-Down>',   '<cmd>resize -2<CR>',          desc = 'Decrease window height', silent = true },
        { 'n',                  '<C-S-Left>',   '<cmd>vertical resize +2<CR>', desc = 'Decrease window width',  silent = true },
        { 'n',                  '<C-S-Right>',  '<cmd>vertical resize -2<CR>', desc = 'Increase window width',  silent = true },

        { 'n',                  '<C-Right>',    'w',                           desc = 'Move one word forward' },
        { 'n',                  '<C-Left>',     'b',                           desc = 'Move one word back' },

        { 'n',                  '<C-Del>',      'de',                          desc = 'Delete word', noremap = true },
        { 'i',                  '<C-Del>',      '<ESC>ldei',                   desc = 'Delete word', noremap = true },
        { 'i',                  '<C-BS>',       '<C-W>',                       desc = 'Delete word', noremap = true },

        { { 'i', 'n', 'v' },    '<C-q>',        '<ESC>ggVG',                   desc = 'Select All',              silent = true, noremap = true },
        { { 'n', 'v' },         '\\',           ':',                           desc = 'Enter command mode',      silent = false },
        { { 'n', 'v' },         '<Space>',      '<Nop>',                       desc = 'Disable <space> (<nop>)', silent = true },
        { 'n',                  't',            'i <ESC>l',                    desc = 'Add space' },

        { 'i',                  '<Home>',       '<ESC>I',                      desc = 'Move to the start of the line' },
        { 'i',                  '<End>',        '<ESC>A',                      desc = 'Move to the end of the line'   },
        { 'n',                  '<Home>',       '^',                           desc = 'Move to the start of the line' },
        { 'n',                  '<End>',        '$',                           desc = 'Move to the ernd of the line'  },

        { 'i',                  '<C-e>',        '<ESC>A',                      desc = 'Move to the end of the line',   silent = true, noremap = true },
        { 'n',                  '<C-e>',        '$',                           desc = 'Move to the end of the line',   silent = true, noremap = true },
        { 'i',                  '<C-a>',        '<ESC>I',                      desc = 'Move to the start of the line', silent = true, noremap = true },

        { 'n',                  'J',            'mzJ`z',                       desc = 'Join lines',                    silent = true },
        { 'n',                  'n',            'nzzzv',                       desc = 'Search next',                   silent = true },
        { 'n',                  'N',            'Nzzzv',                       desc = 'Search previous',               silent = true },

        { { 'i', 'n' },         '<ESC>',        '<cmd>noh<CR><ESC>',           desc = 'Escape and clear search' },
        { { 'i', 'n' },         '<C-c>',        '<cmd>noh<CR><ESC>',           desc = 'Escape and clear search' },

        -- { 'n',                  '<A-j>',        '<cmd>m .+1<CR>==',            desc = 'Move down' },
        -- { 'n',                  '<A-k>',        '<cmd>m .-2<CR>==',            desc = 'Move up'   },
        -- { 'i',                  '<A-j>',        '<ESC><cmd>m .+1<CR>==gi',     desc = 'Move down' },
        -- { 'i',                  '<A-k>',        '<ESC><cmd>m .-2<CR>==gi',     desc = 'Move up'   },
        -- { 'v',                  '<A-j>',        ":m '>+1<CR>gv=gv",            desc = 'Move down' },
        -- { 'v',                  '<A-k>',        ":m '<-2<CR>gv=gv",            desc = 'Move up'   },

        { { 'n', 'i', 'v' },    '<C-PageDown>', '<cmd>bprevious<CR>',          desc = 'Prev buffer' },
        { { 'n', 'i', 'v' },    '<C-PageUp>',   '<cmd>bnext<CR>',              desc = 'Next buffer' },
        { { 'n', 'v' },         '[b',           '<cmd>bprevious<CR>',          desc = 'Prev buffer' },
        { { 'n', 'v' },         ']b',           '<cmd>bnext<CR>',              desc = 'Next buffer' },

        -- { 'n',                  '`',            '<cmd>e #<CR>',                desc = 'Switch to previous buffer'   },
        { { 'n', 'v' },         '[q',           '<cmd>cprev<CR>',              desc = 'Previous quickfix list item' },
        { { 'n', 'v' },         ']q',           '<cmd>cnext<CR>',              desc = 'Next quickfix list item'     },

        {
            'n',
            'gM',
            ":<C-u>call append(line('.') - 1, repeat([''], v:count1))<CR>",
            desc    = 'Insert empty line above current line',
            silent  = true,
            noremap = true
        },
        {
            'n',
            'gm',
            ":<C-u>call append(line('.'), repeat([''], v:count1))<CR>",
            desc    = 'Insert empty line below current line',
            silent  = true,
            noremap = true
        },

        {
            'n',
            'Q',
            closer.close,
            desc = 'Close buffer',
        },

        --
        -- Quiting Neovim
        --
        {
            { 'n', 'v', 'i', 'x' },
            '<A-ESC>',
            '<cmd>qa!<CR>',
            desc   = 'Quit neovim without saving from any mode',
            silent = true,
        },
        { { 'n', 'v' }, 'ZX', '<cmd>qa!<CR>',  desc = 'Quit without saving' },
        { { 'n', 'v' }, 'ZZ', '<cmd>wqa!<CR>', desc = 'Save and Quit'       },
    }
end)(),
    function (item)
        local parts = vim.split(item, ':')
        local mode  = parts[1] or ''
        local keys  = parts[2] or ''
        local cmd   = parts[3] or ''
        local desc  = parts[4] or cmd
        local flags = parts[5] or ''

        if mode == '' or keys == '' or cmd == '' then
            return nil
        end

        local modes = {}
        local _     = mode:gsub(
            '.',
            function (chr)
                table.insert(modes, chr)
            end
        )

        return {
            modes,
            keys,
            function ()
                -- flags:
                -- ------
                --   h=hold
                --   w=wrap
                --   p=popup
                local command = string.format(cmd, vim.fn.expand('%'))
                if string.match(flags, 'h') then
                    command = command .. '; exec ' .. (vim.o.shell or 'bash')
                end

                if flags == '1' or string.match(flags, 'w') then
                    command = string.format('%s/scripts/cmd.sh "%s"', vim.fn.stdpath('config'), command)
                end

                if string.match(flags, 'p') then
                    require('component.util').float_term(vim.split(command, ' '))
                else
                    vim.fn.jobstart(
                        command,
                        {
                            stderr_buffered = true,
                            on_stderr       = function (_, data)
                                require('component.util').notify.err(table.concat(data, '\n'))
                            end,

                            on_exit = function (_, exit_code)
                                if exit_code ~= 0 then
                                    require('component.util').notify.err(string.format(
                                        'Command failed with exit code "%s"',
                                        exit_code
                                    ))
                                end

                                vim.api.nvim_command('checktime')
                            end,
                        }
                    )
                end
            end,
            desc = desc,
        }
    end,
    vim.split(vim.env.NVIM_CUSTOM_COMMANDS or '', '='),
    {
        { keys = '<leader>ss', paths = { 'src', 'private/lib', 'lib', } },
        { keys = '<leader>sl', paths = { 'vendor', 'node_modules'     } },
    }
)
