return {
    {
        'BufEnter',
        function ()
            vim.api.nvim_command('checktime')

            if vim.bo.buftype ~= '' or vim.b.bufentered then
                return
            end

            vim.b.bufentered = true

            local fn = vim.fn.expand('%') or ''
            if fn == '' or vim.fn.getfsize(fn) == 0 then
                vim.api.nvim_command('startinsert')
            end
        end,
    },
    {
        'BufDelete',
        function (event)
            require('component.stack').add('buffers', vim.api.nvim_buf_get_name(event.buf))
        end,
    },
    {
        'ColorScheme',
        require('config.initialize').init,
    },
    {
        { 'BufNewFile', 'BufRead' },
        function ()
            vim.bo.filetype = 'json'
        end,
        pattern = '*.lock',
    },
    {
        'InsertEnter',
        function ()
            if vim.b.changed_number then
                vim.opt_local.relativenumber = false
            end
        end,
    },
    {
        'InsertLeave',
        function (event)
            if vim.bo[event.buf].buflisted and vim.api.nvim_buf_is_loaded(event.buf) and vim.api.nvim_buf_is_valid(event.buf) then
                vim.opt_local.relativenumber = true
                vim.b.changed_number         = true
            end
        end,
    },
    {
        'TextYankPost',
        function ()
            vim.highlight.on_yank({ higroup = 'YankColor', timeout = 200 })
        end,
        silent = true,
    },
    {
        'FileType',
        function ()
            vim.opt_local.commentstring = '// %s'
        end,
        pattern = { 'c', 'java', 'javascript', 'php' },
    },
    {
        'FileType',
        function (event)
            require('component.configurator').keymaps({
                { 'n', '<ESC>', '<cmd>close<CR>', buffer = event.buf, silent = true, desc = 'Close current buffer' },
                { 'n', 'Q',     '<cmd>close<CR>', buffer = event.buf, silent = true, desc = 'Close current buffer' },
                { 'n', 'q',     '<cmd>close<CR>', buffer = event.buf, silent = true, desc = 'Close current buffer' },
            })
        end,
        pattern = {
            'PlenaryTestPopup', 'TelescopeResults', 'TelescopePrompt', 'gitsigns-blame', 'neo-tree-popup',
            'fugitiveblame',    'tsplayground',     'startuptime',     'checkhealth',    'ministarter',
            'dashboard',        'cmp_docs',         'cmp_menu',        'neo-tree',       'WhichKey',
            'lspinfo',          'notify',           'mason',           'noice',          'query',
            'man',              'help',             'lazy',            'git',            'qf',
        },
    },
}
