--                                              
--       ████ ██████           █████      ██
--      ███████████             █████ 
--      █████████ ███████████████████ ███   ███████████
--     █████████  ███    █████████████ █████ ██████████████
--    █████████ ██████████ █████████ █████ █████ ████ █████
--  ███████████ ███    ███ █████████ █████ █████ ████ █████
-- ██████  █████████████████████ ████ █████ █████ ████ ██████
--

local style = require('config.style');

(function (cfg)
    vim.loader.enable()

    vim.api.nvim_command('colorscheme ' .. (vim.env.NVIM_COLORSCHEME or 'habamax'))

    cfg.options(require('config.options'))
    cfg.autocmd({
        'VimEnter',
        function ()
            cfg.autocmds(require('config.autocmds'))

            vim.schedule(function ()
                require('config.initialize').init()
                cfg.keymaps(require('config.keymaps'))
            end)
        end,
        desc = 'Finalyse initializing neovim',
    })
end)(require('component.configurator'));

-- Bootstrap and setup lazy
(function (lazypath)
    if not vim.loop.fs_stat(lazypath) then
        vim.fn.system({ 'git', 'clone', '--filter=blob:none', 'https://github.com/folke/lazy.nvim.git', '--branch=stable', lazypath })
        if vim.v.shell_error ~= 0 then
            vim.notify('Error cloning lazy.nvim', vim.log.levels.ERROR)

            return { setup = function (_) end }
        end
    end

    vim.opt.rtp:prepend(lazypath)

    return require('lazy')
end)(vim.env.NVIM_LAZYDIR or vim.fn.stdpath('data') .. '/lazy/lazy.nvim').setup({
    spec             = { { import = 'plugins-enabled' } },
    rocks            = { enabled  = false, },
    change_detection = { enabled  = false, },
    ui               = {
        size   = { width = 0.8, height = 0.75 },
        border = style.border,
    },
    checker = {
        frequency = 28800, -- Every 8 hours
        notify    = false,
        enabled   = true,
    },
    performance = {
        rtp = {
            disabled_plugins = {
                'rplugin',
                'shada',
                'spellfile',
                'tohtml',
            },
        },
    },
})
