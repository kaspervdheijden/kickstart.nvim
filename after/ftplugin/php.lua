local query_ts = function (query, filetype, root, bufnr)
    local ok, ts_query = pcall(vim.treesitter.query.parse, filetype, query)
    if not ok or ts_query == nil then
        return ''
    end

    for _, captures, _ in ts_query:iter_matches(root, bufnr) do
        return vim.treesitter.get_node_text(captures[1], bufnr) or ''
    end

    return ''
end

local cfg = require('component.configurator')
local buf = vim.api.nvim_get_current_buf()

vim.opt_local.iskeyword:append('$')

cfg.keymaps({
    {
        'n',
        '<leader>c0',
        'e2B"ayt f$l"bywGO<ESC>opublic function get(): <ESC>"ap2B2l"bpB3l~o{<ESC>oreturn $this-><ESC>"bpA;<ESC>o}<ESC><C-o>',
        desc   = 'Create getter: identifier under cursor',
        silent = true,
        buffer = buf,
    },
    {
        'n',
        '<leader>c1',
        [[gg/namespace <CR>V:s/\//\\/g<CR>V:s/private\\lib\\//<CR>V:s/src\\//<CR>V:s/^\\\\ //<CR>V:s/\.[A-Za-z]+;/;/<CR>]],
        desc   = 'Path to namespace',
        silent = true,
        buffer = buf,
    },
    {
        'n',
        '<leader>cc',
        function ()
            require('component.util').copy_and_notify(vim.b.fqclassname or '', 'classname')
        end,
        desc = 'Copy classname',
    },
})

cfg.autocmd({
    { 'BufEnter', 'FocusGained', 'BufModifiedSet' },
    function ()
        local ok, tree = pcall(vim.treesitter.get_parser, buf, 'php')

        if not ok or tree == nil or type(tree.parse) ~= 'function' then
            return
        end

        local names = { 'name', 'identifier' }
        local root  = tree:parse()[1]:root()

        for _, name in ipairs(names) do
            local cls = query_ts('(class_declaration (' .. name .. ') @name)', 'php', root, buf)

            if cls ~= '' then
                local ns = query_ts('(namespace_definition (namespace_name) @name)', 'php', root, buf) .. '\\'

                vim.b[buf].fqclassname = string.gsub(ns, '^\\', '') .. cls
                break
            end
        end
    end,
    desc   = 'Query fully-qualified classname for PHP file',
    buffer = buf,
})
